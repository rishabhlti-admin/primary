create or replace package  body int_opportunity_master_api
is
 
  gc_scope_prefix constant VARCHAR2(31) := lower($$PLSQL_UNIT) || '.';

    procedure get_row (
        p_id                           in number,
        P_project_name                 out varchar2,
        P_project_desc                 out varchar2,
        P_no_of_positions              out number,
        P_start_date                   out date,
        P_end_date                     out date,
        P_int_client_master_id         out number,
        P_fp_tbm                       out varchar2,
        P_spoc                         out varchar2
    )
    is
        l_scope logger_logs.scope%type := gc_scope_prefix || 'get_row';
        l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_id - ', p_id);
        logger.append_param(l_params, 'P_project_name - ', P_project_name);
        logger.append_param(l_params, 'P_project_desc - ', P_project_desc);
        logger.append_param(l_params, 'P_no_of_positions - ', P_no_of_positions);
        logger.append_param(l_params, 'P_start_date - ', P_start_date);
        logger.append_param(l_params, 'P_end_date - ', P_end_date);
        logger.append_param(l_params, 'P_int_client_master_id - ', P_int_client_master_id);
        logger.append_param(l_params, 'P_fp_tbm - ', P_fp_tbm);
        logger.append_param(l_params, 'P_spoc - ', P_spoc);
        for c1 in (select * from int_opportunity_master where int_opportunity_master_id = p_id) loop
            p_project_name := c1.project_name;
            p_project_desc := c1.project_desc;
            p_no_of_positions := c1.no_of_positions;
            p_start_date := c1.start_date;
            p_end_date := c1.end_date;
            p_int_client_master_id := c1.int_client_master_id;
            p_fp_tbm := c1.fp_tbm;
            p_spoc := c1.spoc;
        end loop;
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end get_row;

 
    procedure insert_row  (
        p_id                           in number default null,
        p_project_name                 in varchar2 default null,
        p_project_desc                 in varchar2 default null,
        p_no_of_positions              in number default null,
        p_start_date                   in date default null,
        p_end_date                     in date default null,
        p_int_client_master_id         in number default null,
        p_fp_tbm                       in varchar2 default null,
        p_spoc                         in varchar2 default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'insert_row';
    l_params logger.tab_param;
    v_id int_opportunity_master.int_opportunity_master_id%type;
    begin
        logger.append_param(l_params, 'p_id - ', p_id);
        logger.append_param(l_params, 'P_project_name - ', P_project_name);
        logger.append_param(l_params, 'P_project_desc - ', P_project_desc);
        logger.append_param(l_params, 'P_no_of_positions - ', P_no_of_positions);
        logger.append_param(l_params, 'P_start_date - ', P_start_date);
        logger.append_param(l_params, 'P_end_date - ', P_end_date);
        logger.append_param(l_params, 'P_int_client_master_id - ', P_int_client_master_id);
        logger.append_param(l_params, 'P_fp_tbm - ', P_fp_tbm);
        logger.append_param(l_params, 'P_spoc - ', P_spoc);
        logger.log('START', l_scope, null, l_params);
        insert into int_opportunity_master (
            int_opportunity_master_id,
            project_name,
            project_desc,
            no_of_positions,
            start_date,
            end_date,
            int_client_master_id,
            fp_tbm,
            spoc
        ) values (
            p_id,
            p_project_name,
            p_project_desc,
            p_no_of_positions,
            p_start_date,
            p_end_date,
            p_int_client_master_id,
            p_fp_tbm,
            p_spoc
        )returning int_opportunity_master_id into v_id;
        logger.log_info('Opportunity Master Created', l_scope, 'Opportunity Master created in int_opportunity_master with int_opportunity_master_id = ' || v_id, l_params);
        logger.log('END', l_scope);
        exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end insert_row;

    procedure update_row  (
        p_id                           in number default null,
        p_project_name                 in varchar2 default null,
        p_project_desc                 in varchar2 default null,
        p_no_of_positions              in number default null,
        p_start_date                   in date default null,
        p_end_date                     in date default null,
        p_int_client_master_id         in number default null,
        p_fp_tbm                       in varchar2 default null,
        p_spoc                         in varchar2 default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'update_row';
    l_params logger.tab_param;

    begin
        logger.append_param(l_params, 'p_id - ', p_id);
        logger.append_param(l_params, 'P_project_name - ', P_project_name);
        logger.append_param(l_params, 'P_project_desc - ', P_project_desc);
        logger.append_param(l_params, 'P_no_of_positions - ', P_no_of_positions);
        logger.append_param(l_params, 'P_start_date - ', P_start_date);
        logger.append_param(l_params, 'P_end_date - ', P_end_date);
        logger.append_param(l_params, 'P_int_client_master_id - ', P_int_client_master_id);
        logger.append_param(l_params, 'P_fp_tbm - ', P_fp_tbm);
        logger.append_param(l_params, 'P_spoc - ', P_spoc);
        logger.log('START', l_scope, null, l_params);
        update  int_opportunity_master set 
            int_opportunity_master_id = p_id,
            project_name = p_project_name,
            project_desc = p_project_desc,
            no_of_positions = p_no_of_positions,
            start_date = p_start_date,
            end_date = p_end_date,
            int_client_master_id = p_int_client_master_id,
            fp_tbm = p_fp_tbm,
            spoc = p_spoc
        where int_opportunity_master_id = p_id;
        logger.log_info('Opportunity Master Updated', l_scope, 'Opportunity Master updated in int_opportunity_master with int_opportunity_master_id = ' || p_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end update_row;

    procedure delete_row (
        p_id                           in number
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'delete_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_id - ', p_id);
        logger.log('START', l_scope, null, l_params);
        delete from int_opportunity_master where int_opportunity_master_id = p_id;
        logger.log_info('Opportunity Master Deleted', l_scope, 'Opportunity Master deleted in int_opportunity_master with int_opportunity_master_id = ' || p_id, l_params);
        logger.log('END', l_scope);
        exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end delete_row;

end int_opportunity_master_api;
/
