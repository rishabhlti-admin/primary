create or replace package  body int_practice_api
is

-- =============================================
-- Author:      Rishabh Chowdhury
-- Create date: 26-Jan-2022
-- Description: int_practice_api is packages containing crud   
--              api for int_practice.
-- =============================================

    gc_scope_prefix constant VARCHAR2(31) := lower($$PLSQL_UNIT) || '.';
    
    procedure get_row (
        p_id                           in number,
        P_int_business_unit_id         out number,
        P_practice_name                out varchar2,
        P_practice_head                out varchar2,
        P_head_email                   out varchar2
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'get_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_id - ', p_id);
        logger.append_param(l_params, 'P_int_business_unit_id - ', P_int_business_unit_id);
        logger.append_param(l_params, 'P_practice_name - ', P_practice_name);
        logger.append_param(l_params, 'P_practice_head - ', P_practice_head);
        logger.append_param(l_params, 'P_head_email - ', P_head_email);
        for c1 in (select * from int_practice where int_practice_id = p_id) loop
            p_int_business_unit_id := c1.int_business_unit_id;
            p_practice_name := c1.practice_name;
            p_practice_head := c1.practice_head;
            p_head_email := c1.head_email;
        end loop;
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end get_row;

 
    procedure insert_row  (
        p_id                           in number default null,
        p_int_business_unit_id         in number default null,
        p_practice_name                in varchar2 default null,
        p_practice_head                in varchar2 default null,
        p_head_email                   in varchar2 default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'insert_row';
    l_params logger.tab_param;
    v_id int_practice.int_practice_id%type;
    begin
        logger.append_param(l_params, 'p_id - ', p_id);
        logger.append_param(l_params, 'P_int_business_unit_id - ', P_int_business_unit_id);
        logger.append_param(l_params, 'P_practice_name - ', P_practice_name);
        logger.append_param(l_params, 'P_practice_head - ', P_practice_head);
        logger.append_param(l_params, 'P_head_email - ', P_head_email);
        logger.log('START', l_scope, null, l_params);
        insert into int_practice (
            int_practice_id,
            int_business_unit_id,
            practice_name,
            practice_head,
            head_email
        ) values (
            p_id,
            p_int_business_unit_id,
            p_practice_name,
            p_practice_head,
            p_head_email
        ) returning int_practice_id into v_id;
        logger.log_info('Practice Created', l_scope, 'Practice created in int_practice with int_practice_id = ' || v_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end insert_row;

    procedure update_row  (
        p_id                           in number default null,
        p_int_business_unit_id         in number default null,
        p_practice_name                in varchar2 default null,
        p_practice_head                in varchar2 default null,
        p_head_email                   in varchar2 default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'update_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_id - ', p_id);
        logger.append_param(l_params, 'P_int_business_unit_id - ', P_int_business_unit_id);
        logger.append_param(l_params, 'P_practice_name - ', P_practice_name);
        logger.append_param(l_params, 'P_practice_head - ', P_practice_head);
        logger.append_param(l_params, 'P_head_email - ', P_head_email);
        logger.log('START', l_scope, null, l_params);
        update  int_practice set 
            int_business_unit_id = p_int_business_unit_id,
            practice_name = p_practice_name,
            practice_head = p_practice_head,
            head_email = p_head_email
        where int_practice_id = p_id;
        logger.log_info('Practice Updated', l_scope, 'Practice updated in int_practice with int_practice_id = ' || p_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end update_row;

    procedure delete_row (
        p_id                           in number
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'delete_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_id - ', p_id);
        logger.log('START', l_scope, null, l_params);
        delete from int_practice where int_practice_id = p_id;
        logger.log_info('Practice Deleted', l_scope, 'Practice deleted in int_practice with int_practice_id = ' || p_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end delete_row;

end int_practice_api;
/