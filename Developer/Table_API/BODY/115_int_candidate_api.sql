create or replace package  body int_candidate_api
is
 
 gc_scope_prefix constant VARCHAR2(31) := lower($$PLSQL_UNIT) || '.';
 
    procedure get_row (
        p_int_candidate_id             in  number,
        p_first_name                   out varchar2,
        p_last_name                    out varchar2,
        p_date_of_birth                out date,
        p_emailid                      out varchar2,
        p_gender                       out varchar2,
        p_phone_number                 out number,
        p_city                         out varchar2,
        p_zip_code                     out number,
        p_state                        out varchar2,
        p_country                      out varchar2,
        p_nationality                  out varchar2,
        p_primary_education            out number,
        p_secondary_education          out number,
        p_graduation                   out number,
        p_post_graduation              out number,
        p_total_experience             out number
    )
    is
        l_scope logger_logs.scope%type := gc_scope_prefix || 'get_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_int_candidate_id - ', p_int_candidate_id);
        logger.append_param(l_params, 'p_first_name - ', p_first_name);
        logger.append_param(l_params, 'p_last_name - ', p_last_name);
        logger.append_param(l_params, 'p_date_of_birth - ', p_date_of_birth);
        logger.append_param(l_params, 'p_emailid - ', p_emailid);
        logger.append_param(l_params, 'p_gender - ', p_gender);
        logger.append_param(l_params, 'p_phone_number - ', p_phone_number);
        logger.append_param(l_params, 'p_city - ', p_city);
        logger.append_param(l_params, 'p_zip_code - ', p_zip_code);
        logger.append_param(l_params, 'p_state - ', p_state);
        logger.append_param(l_params, 'p_country - ', p_country);
        logger.append_param(l_params, 'p_nationality - ', p_nationality);
        logger.append_param(l_params, 'p_primary_education - ', p_primary_education);
        logger.append_param(l_params, 'p_secondary_education - ', p_secondary_education);
        logger.append_param(l_params, 'p_graduation - ', p_graduation);
        logger.append_param(l_params, 'p_post_graduation - ', p_post_graduation);
        logger.append_param(l_params, 'p_total_experience - ', p_total_experience);
        for c1 in (select * from int_candidate where int_candidate_id = p_int_candidate_id) loop
            p_first_name := c1.first_name;
            p_last_name := c1.last_name;
            p_date_of_birth := c1.date_of_birth;
            p_emailid := c1.emailid;
            p_gender := c1.gender;
            p_phone_number := c1.phone_number;
            p_city := c1.city;
            p_zip_code := c1.zip_code;
            p_state := c1.state;
            p_country := c1.country;
            p_nationality := c1.nationality;
            p_primary_education := c1.primary_education;
            p_secondary_education := c1.secondary_education;
            p_graduation := c1.graduation;
            p_post_graduation := c1.post_graduation;
            p_total_experience := c1.total_experience;
        end loop;
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end get_row;


    procedure insert_row  (
        p_int_candidate_id             in number default null,
        p_first_name                   in varchar2 default null,
        p_last_name                    in varchar2 default null,
        p_date_of_birth                in date default null,
        p_emailid                      in varchar2 default null,
        p_gender                       in varchar2 default null,
        p_phone_number                 in number default null,
        p_city                         in varchar2 default null,
        p_zip_code                     in number default null,
        p_state                        in varchar2 default null,
        p_country                      in varchar2 default null,
        p_nationality                  in varchar2 default null,
        p_primary_education            in number default null,
        p_secondary_education          in number default null,
        p_graduation                   in number default null,
        p_post_graduation              in number default null,
        p_total_experience             in number default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'insert_row';
    l_params logger.tab_param;
    v_id int_candidate.int_candidate_id%type;
    begin
        logger.append_param(l_params, 'p_int_candidate_id - ', p_int_candidate_id);
        logger.append_param(l_params, 'p_first_name - ', p_first_name);
        logger.append_param(l_params, 'p_last_name - ', p_last_name);
        logger.append_param(l_params, 'p_date_of_birth - ', p_date_of_birth);
        logger.append_param(l_params, 'p_emailid - ', p_emailid);
        logger.append_param(l_params, 'p_gender - ', p_gender);
        logger.append_param(l_params, 'p_phone_number - ', p_phone_number);
        logger.append_param(l_params, 'p_city - ', p_city);
        logger.append_param(l_params, 'p_zip_code - ', p_zip_code);
        logger.append_param(l_params, 'p_state - ', p_state);
        logger.append_param(l_params, 'p_country - ', p_country);
        logger.append_param(l_params, 'p_nationality - ', p_nationality);
        logger.append_param(l_params, 'p_primary_education - ', p_primary_education);
        logger.append_param(l_params, 'p_secondary_education - ', p_secondary_education);
        logger.append_param(l_params, 'p_graduation - ', p_graduation);
        logger.append_param(l_params, 'p_post_graduation - ', p_post_graduation);
        logger.append_param(l_params, 'p_total_experience - ', p_total_experience);
        logger.log('START', l_scope, null, l_params);
        insert into int_candidate (
            int_candidate_id,
            first_name,
            last_name,
            date_of_birth,
            emailid,
            gender,
            phone_number,
            city,
            zip_code,
            state,
            country,
            nationality,
            primary_education,
            secondary_education,
            graduation,
            post_graduation,
            total_experience
        ) values (
            p_int_candidate_id,
            p_first_name,
            p_last_name,
            p_date_of_birth,
            p_emailid,
            p_gender,
            p_phone_number,
            p_city,
            p_zip_code,
            p_state,
            p_country,
            p_nationality,
            p_primary_education,
            p_secondary_education,
            p_graduation,
            p_post_graduation,
            p_total_experience
        )returning int_candidate_id into v_id;
        logger.log_info('Candidate Entry Created', l_scope, 'Candidate created in int_candidate with int_candidate_id = ' || v_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;    
    end insert_row;

    procedure update_row  (
        p_int_candidate_id             in number default null,
        p_first_name                   in varchar2 default null,
        p_last_name                    in varchar2 default null,
        p_date_of_birth                in date default null,
        p_emailid                      in varchar2 default null,
        p_gender                       in varchar2 default null,
        p_phone_number                 in number default null,
        p_city                         in varchar2 default null,
        p_zip_code                     in number default null,
        p_state                        in varchar2 default null,
        p_country                      in varchar2 default null,
        p_nationality                  in varchar2 default null,
        p_primary_education            in number default null,
        p_secondary_education          in number default null,
        p_graduation                   in number default null,
        p_post_graduation              in number default null,
        p_total_experience             in number default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'update_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_int_candidate_id - ', p_int_candidate_id);
        logger.append_param(l_params, 'p_first_name - ', p_first_name);
        logger.append_param(l_params, 'p_last_name - ', p_last_name);
        logger.append_param(l_params, 'p_date_of_birth - ', p_date_of_birth);
        logger.append_param(l_params, 'p_emailid - ', p_emailid);
        logger.append_param(l_params, 'p_gender - ', p_gender);
        logger.append_param(l_params, 'p_phone_number - ', p_phone_number);
        logger.append_param(l_params, 'p_city - ', p_city);
        logger.append_param(l_params, 'p_zip_code - ', p_zip_code);
        logger.append_param(l_params, 'p_state - ', p_state);
        logger.append_param(l_params, 'p_country - ', p_country);
        logger.append_param(l_params, 'p_nationality - ', p_nationality);
        logger.append_param(l_params, 'p_primary_education - ', p_primary_education);
        logger.append_param(l_params, 'p_secondary_education - ', p_secondary_education);
        logger.append_param(l_params, 'p_graduation - ', p_graduation);
        logger.append_param(l_params, 'p_post_graduation - ', p_post_graduation);
        logger.append_param(l_params, 'p_total_experience - ', p_total_experience);
        logger.log('START', l_scope, null, l_params);
   
        update  int_candidate set 
            int_candidate_id = p_int_candidate_id,
            first_name = p_first_name,
            last_name = p_last_name,
            date_of_birth = p_date_of_birth,
            emailid = p_emailid,
            gender = p_gender,
            phone_number = p_phone_number,
            city = p_city,
            zip_code = p_zip_code,
            state = p_state,
            country = p_country,
            nationality = p_nationality,
            primary_education = p_primary_education,
            secondary_education = p_secondary_education,
            graduation = p_graduation,
            post_graduation = p_post_graduation,
            total_experience = p_total_experience
        where int_candidate_id = p_int_candidate_id;
        logger.log_info('Candidate Entry Updated', l_scope, 'Candidate updated in int_candidate with int_candidate_id = ' || p_int_candidate_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end update_row;

    procedure delete_row (
        p_int_candidate_id             in number
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'delete_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_int_candidate_id - ', p_int_candidate_id);
        logger.log('START', l_scope, null, l_params);
        delete from int_candidate where int_candidate_id = p_int_candidate_id;
        logger.log_info('Candidate Deleted', l_scope, 'Candidate deleted in int_candidate with int_candidate_id = ' || p_int_candidate_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end delete_row;

end int_candidate_api;
