create or replace package  body int_candidate_experience_api
is

 gc_scope_prefix constant VARCHAR2(31) := lower($$PLSQL_UNIT) || '.';
 
    procedure get_row (
        p_int_candidate_experience_id        in  number,
        p_candidate_id                	   out number,
        p_experience                  	   out number,
        p_company_name                	   out varchar2,
        p_start_date                  	   out date,
        p_end_date                    	   out date
    )
    is
    
        l_scope logger_logs.scope%type := gc_scope_prefix || 'get_row';
        l_params logger.tab_param;
        
    begin
    
        logger.append_param(l_params, 'p_int_candidate_experience_id - ', p_int_candidate_experience_id);
        logger.append_param(l_params, 'p_candidate_id - ', p_candidate_id);
        logger.append_param(l_params, 'p_experience - ', p_experience);
        logger.append_param(l_params, 'p_company_name - ', p_company_name);
        logger.append_param(l_params, 'p_start_date - ', p_start_date);
        logger.append_param(l_params, 'p_end_date - ', p_end_date);  
        for c1 in (select * from int_candidate_experience where int_candidate_experience_id = p_int_candidate_experience_id) loop
            p_candidate_id := c1.candidate_id;
            p_experience := c1.experience;
            p_company_name := c1.company_name;
            p_start_date := c1.start_date;
            p_end_date := c1.end_date;
        end loop;
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;        
    end get_row;


    procedure insert_row  (
        p_int_candidate_experience_id        in number default null,
        p_candidate_id                 	   in number default null,
        p_experience                  	   in number default null,
        p_company_name                	   in varchar2 default null,
        p_start_date                  	   in date default null,
        p_end_date                    	   in date default null
    )
    is
    
    l_scope logger_logs.scope%type := gc_scope_prefix || 'insert_row';
    l_params logger.tab_param;
    v_id int_candidate_experience.int_candidate_experience_id%type;    
    
    
    begin   
        logger.append_param(l_params, 'p_int_candidate_experience_id - ', p_int_candidate_experience_id);
        logger.append_param(l_params, 'p_candidate_id - ', p_candidate_id);
        logger.append_param(l_params, 'p_experience - ', p_experience);
        logger.append_param(l_params, 'p_company_name - ', p_company_name);
        logger.append_param(l_params, 'p_start_date - ', p_start_date);
        logger.append_param(l_params, 'p_end_date - ', p_end_date);
        logger.log('START', l_scope, null, l_params);
        insert into int_candidate_experience (
            int_candidate_experience_id,
            candidate_id,
            experience,
            company_name,
            start_date,
            end_date
        ) values (
            p_int_candidate_experience_id,
            p_candidate_id,
            p_experience,
            p_company_name,
            p_start_date,
            p_end_date
        )returning int_candidate_experience_id into v_id;
        logger.log_info('Candidate Experience Entry Created', l_scope, 'Candidate Experience created in int_candidate_experience with int_candidate_experience_id = ' || v_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end insert_row;

    procedure update_row  (
        p_int_candidate_experience_id        in number default null,
        p_candidate_id                 	   in number default null,
        p_experience                   	   in number default null,
        p_company_name                	   in varchar2 default null,
        p_start_date                 	   in date default null,
        p_end_date                    	   in date default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'update_row';
    l_params logger.tab_param;   
    begin
    
        logger.append_param(l_params, 'p_int_candidate_experience_id - ', p_int_candidate_experience_id);
        logger.append_param(l_params, 'p_candidate_id - ', p_candidate_id);
        logger.append_param(l_params, 'p_experience - ', p_experience);
        logger.append_param(l_params, 'p_company_name - ', p_company_name);
        logger.append_param(l_params, 'p_start_date - ', p_start_date);
        logger.append_param(l_params, 'p_end_date - ', p_end_date);
        logger.log('START', l_scope, null, l_params);    
        update  int_candidate_experience set 
            int_candidate_experience_id = p_int_candidate_experience_id,
            candidate_id = p_candidate_id,
            experience = p_experience,
            company_name = p_company_name,
            start_date = p_start_date,
            end_date = p_end_date
        where int_candidate_experience_id = p_int_candidate_experience_id;
        logger.log_info('Candidate Experience Entry Updated', l_scope, 'Candidate Experience updated in int_candidate_experience with int_candidate_experience_id = ' || p_int_candidate_experience_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;        
    end update_row;

    procedure delete_row (
        p_int_candidate_experience_id        in number
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'delete_row';
    l_params logger.tab_param;    
    begin
        logger.append_param(l_params, 'p_int_candidate_experience_id - ', p_int_candidate_experience_id);
        logger.log('START', l_scope, null, l_params);    
        delete from int_candidate_experience where int_candidate_experience_id = p_int_candidate_experience_id;
        logger.log_info('Candidate Experience Entry Deleted', l_scope, 'Candidate Experience Entry deleted in int_candidate_experience with int_candidate_experience_id = ' || p_int_candidate_experience_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;        
    end delete_row;

end int_candidate_experience_api;
