create or replace package  body int_candidate_document_api
is

 gc_scope_prefix constant VARCHAR2(31) := lower($$PLSQL_UNIT) || '.';
 
    procedure get_row (
        p_int_candidate_document_id        in  number,
        p_candidate_id                 out number,
        p_document_name                out varchar2,
        p_document                     out blob,
        p_last_updated                 out date
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'get_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_int_candidate_document_id - ', p_int_candidate_document_id);
        logger.append_param(l_params, 'p_candidate_id - ', p_candidate_id);
        logger.append_param(l_params, 'p_document_name - ', p_document_name);
       -- logger.append_param(l_params, 'p_document - ', p_document);
        logger.append_param(l_params, 'p_last_updated - ', p_last_updated);
        for c1 in (select * from int_candidate_document where int_candidate_document_id = p_int_candidate_document_id) loop
            p_candidate_id := c1.candidate_id;
            p_document_name := c1.document_name;
            p_document := c1.document;
            p_last_updated := c1.last_updated;
        end loop;
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;       
    end get_row;


    procedure insert_row  (
        p_int_candidate_document_id       in number default null,
        p_candidate_id                    in number default null,
        p_document_name                   in varchar2 default null,
        p_document                        in blob default null,
        p_last_updated                    in date default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'insert_row';
    l_params logger.tab_param;
    v_id int_candidate_document.int_candidate_document_id%type;    
    begin
        logger.append_param(l_params, 'p_int_candidate_document_id - ', p_int_candidate_document_id);
        logger.append_param(l_params, 'p_candidate_id - ', p_candidate_id);
        logger.append_param(l_params, 'p_document_name - ', p_document_name);
        --logger.append_param(l_params, 'p_document - ', p_document);
        logger.append_param(l_params, 'p_last_updated - ', p_last_updated); 
        logger.log('START', l_scope, null, l_params);
        insert into int_candidate_document (
            int_candidate_document_id,
            candidate_id,
            document_name,
            document,
            last_updated
        ) values (
            p_int_candidate_document_id,
            p_candidate_id,
            p_document_name,
            p_document,
            p_last_updated
        )returning int_candidate_document_id into v_id;
        logger.log_info('Candidate Document Entry Created', l_scope, 'Candidate Document entry created in int_candidate_document with int_candidate_document_id = ' || v_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise; 
    end insert_row;

    procedure update_row  (
        p_int_candidate_document_id        in number default null,
        p_candidate_id                 	 in number default null,
        p_document_name                	 in varchar2 default null,
        p_document                     	 in blob default null,
        p_last_updated                 	 in date default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'update_row';
    l_params logger.tab_param;   
    begin
        logger.append_param(l_params, 'p_int_candidate_document_id - ', p_int_candidate_document_id);
        logger.append_param(l_params, 'p_candidate_id - ', p_candidate_id);
        logger.append_param(l_params, 'p_document_name - ', p_document_name);
        --logger.append_param(l_params, 'p_document - ', p_document);
        logger.append_param(l_params, 'p_last_updated - ', p_last_updated);  
        logger.log('START', l_scope, null, l_params);
        update  int_candidate_document set 
            int_candidate_document_id = p_int_candidate_document_id,
            candidate_id = p_candidate_id,
            document_name = p_document_name,
            document = p_document,
            last_updated = p_last_updated
        where int_candidate_document_id = p_int_candidate_document_id;
        logger.log_info('Candidate Document Updated', l_scope, 'Candidate Document updated in int_candidate_document with int_candidate_document_id = ' || p_int_candidate_document_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end update_row;

    procedure delete_row (
        p_int_candidate_document_id        in number
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'delete_row';
    l_params logger.tab_param;    
    begin
        logger.append_param(l_params, 'p_int_candidate_document_id - ', p_int_candidate_document_id);
        logger.log('START', l_scope, null, l_params);
        delete from int_candidate_document where int_candidate_document_id = p_int_candidate_document_id;
        logger.log_info('Candidate Document Deleted', l_scope, 'Candidate Document deleted in int_candidate_candidate with p_int_candidate_document_id = ' || p_int_candidate_document_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;       
    end delete_row;

end int_candidate_document_api;
