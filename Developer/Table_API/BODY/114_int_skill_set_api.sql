create or replace package  body int_skill_set_api
is
 
    gc_scope_prefix constant VARCHAR2(31) := lower($$PLSQL_UNIT) || '.';
    
    procedure get_row (
        p_int_skill_set_id             in  number,
        p_role_id                      out number,
        p_skill_name                   out varchar2
    )
    is
      l_scope logger_logs.scope%type := gc_scope_prefix || 'get_row';
      l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_int_skill_set_id - ', p_int_skill_set_id);
        logger.append_param(l_params, 'p_role_id - ', p_role_id);
        logger.append_param(l_params, 'p_skill_name - ', p_skill_name);
        for c1 in (select * from int_skill_set where int_skill_set_id = p_int_skill_set_id) loop
            p_role_id := c1.role_id;
            p_skill_name := c1.skill_name;
        end loop;
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end get_row;

 
    procedure insert_row  (
        p_int_skill_set_id             in number default null,
        p_role_id                      in number default null,
        p_skill_name                   in varchar2 default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'insert_row';
    l_params logger.tab_param;
    v_id int_skill_set.int_skill_set_id%type;
    begin
        logger.append_param(l_params, 'p_int_skill_set_id - ', p_int_skill_set_id);
        logger.append_param(l_params, 'p_role_id - ', p_role_id);
        logger.append_param(l_params, 'p_skill_name - ', p_skill_name);
        logger.log('START', l_scope, null, l_params);
        insert into int_skill_set (
            int_skill_set_id,
            role_id,
            skill_name
        ) values (
            p_int_skill_set_id,
            p_role_id,
            p_skill_name
        )returning int_skill_set_id into v_id;
        logger.log_info('Skill Set Created', l_scope, 'Skill set created in int_skill_set with int_skill_set_id = ' || v_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end insert_row;

    procedure update_row  (
        p_int_skill_set_id             in number default null,
        p_role_id                      in number default null,
        p_skill_name                   in varchar2 default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'update_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_int_skill_set_id - ', p_int_skill_set_id);
        logger.append_param(l_params, 'p_role_id - ', p_role_id);
        logger.append_param(l_params, 'p_skill_name - ', p_skill_name);
        logger.log('START', l_scope, null, l_params);
        update  int_skill_set set 
            int_skill_set_id = p_int_skill_set_id,
            role_id = p_role_id,
            skill_name = p_skill_name
        where int_skill_set_id = p_int_skill_set_id;
        logger.log_info('Skill Set Updated', l_scope, 'Skill set updated in int_skill_set with int_skill_set_id = ' || p_int_skill_set_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end update_row;

    procedure delete_row (
        p_int_skill_set_id             in number
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'delete_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_int_skill_set_id - ', p_int_skill_set_id);
        logger.log('START', l_scope, null, l_params);
        delete from int_skill_set where int_skill_set_id = p_int_skill_set_id;
        logger.log_info('Skill Set Deleted', l_scope, 'Skill Set deleted in int_skill_set with int_skill_set_id = ' || p_int_skill_set_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end delete_row;

end int_skill_set_api;
/