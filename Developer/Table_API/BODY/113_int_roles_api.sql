create or replace package  body int_roles_api
is
    gc_scope_prefix constant VARCHAR2(31) := lower($$PLSQL_UNIT) || '.';
     
    procedure get_row (
        p_int_role_id                  in  number,
        p_rolename                     out varchar2
    )
    is
     l_scope logger_logs.scope%type := gc_scope_prefix || 'get_row';
     l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_int_role_id - ', p_int_role_id);
        logger.append_param(l_params, 'p_rolename - ', p_rolename);
        for c1 in (select * from int_roles where int_role_id = p_int_role_id) loop
            p_rolename := c1.rolename;
        end loop;
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end get_row;

 
    procedure insert_row  (
        p_int_role_id                  in number default null,
        p_rolename                     in varchar2 default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'insert_row';
    l_params logger.tab_param;
    v_id int_roles.int_role_id%type;
    begin
        logger.append_param(l_params, 'p_int_role_id - ', p_int_role_id);
        logger.append_param(l_params, 'p_rolename - ', p_rolename);
        logger.log('START', l_scope, null, l_params);
        insert into int_roles (
            int_role_id,
            rolename
        ) values (
            p_int_role_id,
            p_rolename
        )returning p_int_role_id into v_id;
        logger.log_info('Role Created', l_scope, 'Role created in int_roles with int_role_id = ' || v_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end insert_row;

    procedure update_row  (
        p_int_role_id                  in number default null,
        p_rolename                     in varchar2 default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'update_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_int_role_id - ', p_int_role_id);
        logger.append_param(l_params, 'p_rolename - ', p_rolename);
        logger.log('START', l_scope, null, l_params);
        update  int_roles set 
            int_role_id = p_int_role_id,
            rolename = p_rolename
        where int_role_id = p_int_role_id;
          logger.log_info('Roles Updated', l_scope, 'Roles updated in int_roles with int_role_id = ' || p_int_role_id, l_params);
          logger.log('END', l_scope);
        exception
        when others then
          logger.log_error('Unhandled Exception', l_scope, null, l_params);
          raise;
    end update_row;

    procedure delete_row (
        p_int_role_id                  in number
    )
    is
      l_scope logger_logs.scope%type := gc_scope_prefix || 'delete_row';
      l_params logger.tab_param;
    begin
      logger.append_param(l_params, 'p_int_role_id - ', p_int_role_id);
        logger.log('START', l_scope, null, l_params);
        delete from int_roles where int_role_id = p_int_role_id;
        logger.log_info('Role Deleted', l_scope, 'Role deleted in int_roles with int_role_id = ' || p_int_role_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end delete_row;

end int_roles_api;
/