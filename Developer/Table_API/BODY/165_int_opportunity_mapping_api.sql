create or replace package  body int_opportunity_mapping_api
is
 
    gc_scope_prefix constant VARCHAR2(31) := lower($$PLSQL_UNIT) || '.';

    procedure get_row (
        p_id                           in number,
        P_int_opportunity_breakup_id   out number,
        P_experience_in_specified_skill out number,
        P_int_skill_set_id             out number
    )
    is
        l_scope logger_logs.scope%type := gc_scope_prefix || 'get_row';
        l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_id - ', p_id);
        logger.append_param(l_params, 'P_int_opportunity_breakup_id - ', P_int_opportunity_breakup_id);
        logger.append_param(l_params, 'P_experience_in_specified_skill - ', P_experience_in_specified_skill);
        logger.append_param(l_params, 'P_int_skill_set_id - ', P_int_skill_set_id);
        for c1 in (select * from int_opportunity_mapping where int_opportunity_mapping_id = p_id) loop
            p_int_opportunity_breakup_id := c1.int_opportunity_breakup_id;
            p_experience_in_specified_skill := c1.experience_in_specified_skill;
            p_int_skill_set_id := c1.int_skill_set_id;
        end loop;
        exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end get_row;

 
    procedure insert_row  (
        p_id                           in number default null,
        p_int_opportunity_breakup_id   in number default null,
        p_experience_in_specified_skill in number default null,
        p_int_skill_set_id             in number default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'insert_row';
    l_params logger.tab_param;
    v_id int_opportunity_mapping.int_opportunity_mapping_id%type;
    begin
        logger.append_param(l_params, 'p_id - ', p_id);
        logger.append_param(l_params, 'P_int_opportunity_breakup_id - ', P_int_opportunity_breakup_id);
        logger.append_param(l_params, 'P_experience_in_specified_skill - ', P_experience_in_specified_skill);
        logger.append_param(l_params, 'P_int_skill_set_id - ', P_int_skill_set_id);
        logger.log('START', l_scope, null, l_params);
        insert into int_opportunity_mapping (
            INT_OPPORTUNITY_MAPPING_ID,
            int_opportunity_breakup_id,
            experience_in_specified_skill,
            int_skill_set_id
        ) values (
            p_id,
            p_int_opportunity_breakup_id,
            p_experience_in_specified_skill,
            p_int_skill_set_id
        )returning int_opportunity_breakup_id into v_id;
        logger.log_info('Opportunity Mapping Created', l_scope, 'Opportunity Mapping created in int_opportunity_mapping with int_opportunity_mapping_id = ' || v_id, l_params);
        logger.log('END', l_scope);
        exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end insert_row;

    procedure update_row  (
        p_id                           in number default null,
        p_int_opportunity_breakup_id   in number default null,
        p_experience_in_specified_skill in number default null,
        p_int_skill_set_id             in number default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'update_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_id - ', p_id);
        logger.append_param(l_params, 'P_int_opportunity_breakup_id - ', P_int_opportunity_breakup_id);
        logger.append_param(l_params, 'P_experience_in_specified_skill - ', P_experience_in_specified_skill);
        logger.append_param(l_params, 'P_int_skill_set_id - ', P_int_skill_set_id);
        logger.log('START', l_scope, null, l_params);

        update  int_opportunity_mapping set 
            int_opportunity_mapping_id = p_id,
            int_opportunity_breakup_id = p_int_opportunity_breakup_id,
            experience_in_specified_skill = p_experience_in_specified_skill,
            int_skill_set_id = p_int_skill_set_id
        where int_opportunity_mapping_id = p_id;
        logger.log_info('Opportunity Mapping Updated', l_scope, 'Opportunity Mapping updated in int_opportunity_mapping with int_opportunity_mapping_id = ' || p_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;

    end update_row;

    procedure delete_row (
        p_id                           in number
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'delete_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_id - ', p_id);
        logger.log('START', l_scope, null, l_params);
        delete from int_opportunity_mapping where int_opportunity_mapping_id = p_id;
        logger.log_info('Opportunity Mapping Deleted', l_scope, 'Opportunity Mapping deleted in int_opportunity_mapping with int_opportunity_mapping_id = ' || p_id, l_params);
        logger.log('END', l_scope);
        exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;


    end delete_row;

end int_opportunity_mapping_api;
/