create or replace package  body int_opportunity_breakup_api
is
 
   gc_scope_prefix constant VARCHAR2(31) := lower($$PLSQL_UNIT) || '.';
 
    procedure get_row (
        p_id                           in number,
        P_int_opportunity_master_id    out number,
        P_experience                   out number,
        P_cadre                        out varchar2,
        P_location                     out varchar2,
        P_int_practice_id              out number
    )
    is
        l_scope logger_logs.scope%type := gc_scope_prefix || 'get_row';
        l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_id - ', p_id);
        logger.append_param(l_params, 'P_int_opportunity_master_id - ', P_int_opportunity_master_id);
        logger.append_param(l_params, 'P_experience - ', P_experience);
        logger.append_param(l_params, 'P_cadre - ', P_cadre);
        logger.append_param(l_params, 'P_location - ', P_location);
        logger.append_param(l_params, 'P_int_practice_id - ', P_int_practice_id);
        for c1 in (select * from int_opportunity_breakup where int_opportunity_breakup_id = p_id) loop
            p_int_opportunity_master_id := c1.int_opportunity_master_id;
            p_experience := c1.experience;
            p_cadre := c1.cadre;
            p_location := c1.location;
            p_int_practice_id := c1.int_practice_id;
        end loop;
        exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end get_row;

 
    procedure insert_row  (
        p_id                           in number default null,
        p_int_opportunity_master_id    in number default null,
        p_experience                   in number default null,
        p_cadre                        in varchar2 default null,
        p_location                     in varchar2 default null,
        p_int_practice_id              in number default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'insert_row';
    l_params logger.tab_param;
    v_id int_opportunity_breakup.int_opportunity_breakup_id%type;
    begin
        logger.append_param(l_params, 'p_id - ', p_id);
        logger.append_param(l_params, 'P_int_opportunity_master_id - ', P_int_opportunity_master_id);
        logger.append_param(l_params, 'P_experience - ', P_experience);
        logger.append_param(l_params, 'P_cadre - ', P_cadre);
        logger.append_param(l_params, 'P_location - ', P_location);
        logger.append_param(l_params, 'P_int_practice_id - ', P_int_practice_id);
        logger.log('START', l_scope, null, l_params);
        insert into int_opportunity_breakup (
            int_opportunity_breakup_id,
            int_opportunity_master_id,
            experience,
            cadre,
            location,
            int_practice_id
        ) values (
            p_id,
            p_int_opportunity_master_id,
            p_experience,
            p_cadre,
            p_location,
            p_int_practice_id
        )returning int_opportunity_breakup_id into v_id;
        logger.log_info('Opportunity Breakup Created', l_scope, 'Opportunity Breakup created in int_opportunity_breakup with int_opportunity_breakup_id = ' || v_id, l_params);
        logger.log('END', l_scope);
        exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end insert_row;

    procedure update_row  (
        p_id                           in number default null,
        p_int_opportunity_master_id    in number default null,
        p_experience                   in number default null,
        p_cadre                        in varchar2 default null,
        p_location                     in varchar2 default null,
        p_int_practice_id              in number default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'update_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_id - ', p_id);
        logger.append_param(l_params, 'P_int_opportunity_master_id - ', P_int_opportunity_master_id);
        logger.append_param(l_params, 'P_experience - ', P_experience);
        logger.append_param(l_params, 'P_cadre - ', P_cadre);
        logger.append_param(l_params, 'P_location - ', P_location);
        logger.append_param(l_params, 'P_int_practice_id - ', P_int_practice_id);
        logger.log('START', l_scope, null, l_params);
        update  int_opportunity_breakup set 
            int_opportunity_breakup_id = p_id,
            int_opportunity_master_id = p_int_opportunity_master_id,
            experience = p_experience,
            cadre = p_cadre,
            location = p_location,
            int_practice_id = p_int_practice_id
        where int_opportunity_breakup_id = p_id;
        logger.log_info('Opportunity Breakup Updated', l_scope, 'Opportunity Breakup updated in int_opportunity_breakup with int_opportunity_breakup_id = ' || p_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;

    end update_row;

    procedure delete_row (
        p_id                           in number
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'delete_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_id - ', p_id);
        logger.log('START', l_scope, null, l_params);
        delete from int_opportunity_breakup where int_opportunity_breakup_id = p_id;
        logger.log_info('Opportunity Breakup Deleted', l_scope, 'Opportunity Breakup deleted in int_opportunity_breakup with int_opportunity_breakup_id = ' || p_id, l_params);
        logger.log('END', l_scope);
        exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;

    end delete_row;

end int_opportunity_breakup_api;
/
