create or replace package  body int_business_unit_api
is
    gc_scope_prefix constant VARCHAR2(31) := lower($$PLSQL_UNIT) || '.';
    
    procedure get_row (
        p_id                           in number,
        P_bu_name                      out varchar2,
        P_bu_head                      out varchar2
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'get_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_id - ', p_id);
        logger.append_param(l_params, 'p_bu_name - ', p_bu_name);
        logger.append_param(l_params, 'P_bu_head - ', P_bu_head);
        for c1 in (select * from int_business_unit where int_business_unit_id = p_id) loop
            p_bu_name := c1.bu_name;
            p_bu_head := c1.bu_head;
        end loop;
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end get_row;

 
    procedure insert_row  (
        p_id                           in number default null,
        p_bu_name                      in varchar2 default null,
        p_bu_head                      in varchar2 default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'insert_row';
    l_params logger.tab_param;
    v_id int_business_unit.int_business_unit_id%type;
    begin
        logger.append_param(l_params, 'p_id - ', p_id);
        logger.append_param(l_params, 'p_bu_name - ', p_bu_name);
        logger.append_param(l_params, 'P_bu_head - ', P_bu_head);
        logger.log('START', l_scope, null, l_params);
        insert into int_business_unit (
            int_business_unit_id,
            bu_name,
            bu_head
        ) values (
            p_id,
            p_bu_name,
            p_bu_head
        ) returning int_business_unit_id into v_id;
        logger.log_info('BU Created', l_scope, 'BU created in int_business_unit with int_business_unit_id = ' || v_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end insert_row;

    procedure update_row  (
        p_id                           in number default null,
        p_bu_name                      in varchar2 default null,
        p_bu_head                      in varchar2 default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'update_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_id - ', p_id);
        logger.append_param(l_params, 'p_bu_name - ', p_bu_name);
        logger.append_param(l_params, 'P_bu_head - ', P_bu_head);
        logger.log('START', l_scope, null, l_params);
        update  int_business_unit set 
            bu_name = p_bu_name,
            bu_head = p_bu_head
        where int_business_unit_id = p_id;
        logger.log_info('BU Updated', l_scope, 'BU updated in int_business_unit with int_business_unit_id = ' || p_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end update_row;

    procedure delete_row (
        p_id                           in number
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'delete_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_id - ', p_id);
        logger.log('START', l_scope, null, l_params);
        delete from int_business_unit where int_business_unit_id = p_id;
        logger.log_info('BU Deleted', l_scope, 'BU deleted in int_business_unit with int_business_unit_id = ' || p_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end delete_row;

begin
    DBMS_APPLICATION_INFO.set_client_info(client_info => sys_guid() || ':SQL-Developer');
end int_business_unit_api;
/