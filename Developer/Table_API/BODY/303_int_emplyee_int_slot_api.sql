create or replace package  body int_employee_int_slot_api
is
 
    gc_scope_prefix constant VARCHAR2(31) := lower($$PLSQL_UNIT) || '.';
    
    procedure get_row (
        p_int_employee_int_slot_id     in  number,
        p_employee_id                  out number,
        p_interview_dt                 out date,
        p_start_time                   out timestamp,
        p_end_time                     out timestamp,
        p_status                       out varchar2
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'get_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_int_employee_int_slot_id - ', p_int_employee_int_slot_id);
        logger.append_param(l_params, 'p_employee_id - ', p_employee_id);
        logger.append_param(l_params, 'p_interview_dt - ', p_interview_dt);
        logger.append_param(l_params, 'p_start_time - ', p_start_time);
        logger.append_param(l_params, 'p_end_time - ', p_end_time);
        logger.append_param(l_params, 'p_status - ', p_status);
        for c1 in (select * from int_employee_int_slot where int_employee_int_slot_id = p_int_employee_int_slot_id) loop
            p_employee_id := c1.employee_id;
            p_interview_dt := c1.interview_dt;
            p_start_time := c1.start_time;
            p_end_time := c1.end_time;
            p_status := c1.status;
        end loop;
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end get_row;

 
    procedure insert_row  (
        p_int_employee_int_slot_id     in number default null,
        p_employee_id                  in number default null,
        p_interview_dt                 in date default null,
        p_start_time                   in timestamp default null,
        p_end_time                     in timestamp default null,
        p_status                       in varchar2 default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'insert_row';
    l_params logger.tab_param;
    v_id int_employee_int_slot.int_employee_int_slot_id%type;
    begin
        logger.append_param(l_params, 'p_int_employee_int_slot_id - ', p_int_employee_int_slot_id);
        logger.append_param(l_params, 'p_employee_id - ', p_employee_id);
        logger.append_param(l_params, 'p_interview_dt - ', p_interview_dt);
        logger.append_param(l_params, 'p_start_time - ', p_start_time);
        logger.append_param(l_params, 'p_end_time - ', p_end_time);
        logger.append_param(l_params, 'p_status - ', p_status);
        logger.log('START', l_scope, null, l_params);
        insert into int_employee_int_slot (
            employee_id,
            interview_dt,
            start_time,
            end_time,
            status
        ) values (
            p_employee_id,
            p_interview_dt,
            p_start_time,
            p_end_time,
            p_status
        ) returning int_employee_int_slot_id into v_id;
        logger.log_info('Employee slot Created', l_scope, 'Employee slot created in int_employee_int_slot with int_employee_int_slot_id = ' || v_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end insert_row;

    procedure update_row  (
        p_int_employee_int_slot_id     in number default null,
        p_employee_id                  in number default null,
        p_interview_dt                 in date default null,
        p_start_time                   in timestamp default null,
        p_end_time                     in timestamp default null,
        p_status                       in varchar2 default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'update_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_int_employee_int_slot_id - ', p_int_employee_int_slot_id);
        logger.append_param(l_params, 'p_employee_id - ', p_employee_id);
        logger.append_param(l_params, 'p_interview_dt - ', p_interview_dt);
        logger.append_param(l_params, 'p_start_time - ', p_start_time);
        logger.append_param(l_params, 'p_end_time - ', p_end_time);
        logger.append_param(l_params, 'p_status - ', p_status);
        logger.log('START', l_scope, null, l_params);
        update  int_employee_int_slot set 
            int_employee_int_slot_id = p_int_employee_int_slot_id,
            employee_id = p_employee_id,
            interview_dt = p_interview_dt,
            start_time = p_start_time,
            end_time = p_end_time,
            status = p_status
        where int_employee_int_slot_id = p_int_employee_int_slot_id;
        logger.log_info('Employee Slot Updated', l_scope, 'Employee slot updated in int_employee_int_slot with int_employee_int_slot_id = ' || p_int_employee_int_slot_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end update_row;

    procedure delete_row (
        p_int_employee_int_slot_id     in number
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'delete_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_int_employee_int_slot_id - ', p_int_employee_int_slot_id);
        logger.log('START', l_scope, null, l_params);
        delete from int_employee_int_slot where int_employee_int_slot_id = p_int_employee_int_slot_id;
        logger.log_info('Employee Slot Deleted', l_scope, 'Employee slot updated in int_employee_int_slot with int_employee_int_slot_id = ' || p_int_employee_int_slot_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end delete_row;

end int_employee_int_slot_api;
/