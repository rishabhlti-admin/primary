create or replace package  body int_employee_skill_master_api
is
 
    gc_scope_prefix constant VARCHAR2(31) := lower($$PLSQL_UNIT) || '.';
    
    procedure get_row (
        p_int_employee_skill_master_id  in  number,
        p_employee_id                  out number,
        p_rating                       out number,
        p_experience                   out number,
        p_skill_set_id                 out number
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'get_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_int_employee_skill_master_id - ', p_int_employee_skill_master_id);
        logger.append_param(l_params, 'p_employee_id - ', p_employee_id);
        logger.append_param(l_params, 'p_rating - ', p_rating);
        logger.append_param(l_params, 'p_experience - ', p_experience);
        logger.append_param(l_params, 'p_skill_set_id - ', p_skill_set_id);
        for c1 in (select * from int_employee_skill_master where  int_employee_skill_master_id = p_int_employee_skill_master_id) loop
            p_employee_id := c1.employee_id;
            p_rating := c1.rating;
            p_experience := c1.experience;
            p_skill_set_id := c1.skill_set_id;
         end loop;
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end get_row;

 
    procedure insert_row  (
        p_int_employee_skill_master_id        in number default null,
        p_employee_id                  in number default null,
        p_rating                       in number default null,
        p_experience                   in number default null,
        p_skill_set_id                 in number default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'insert_row';
    l_params logger.tab_param;
    v_id int_employee_skill_master.int_employee_skill_master_id%type;
    begin
        logger.append_param(l_params, 'p_int_employee_skill_master_id - ', p_int_employee_skill_master_id);
        logger.append_param(l_params, 'p_employee_id - ', p_employee_id);
        logger.append_param(l_params, 'p_rating - ', p_rating);
        logger.append_param(l_params, 'p_experience - ', p_experience);
        logger.append_param(l_params, 'p_skill_set_id - ', p_skill_set_id);
        logger.log('START', l_scope, null, l_params);
        insert into int_employee_skill_master (
            int_employee_skill_master_id,
            employee_id,
            rating,
            experience,
            skill_set_id
        ) values (
            p_int_employee_skill_master_id,
            p_employee_id,
            p_rating,
            p_experience,
            p_skill_set_id
        )returning int_employee_skill_master_id into v_id;
        logger.log_info('Employee Skill Created', l_scope, 'Employee Skill created in int_employee_skill_master with int_employee_skill_master_id = ' || v_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end insert_row;

    procedure update_row  (
        p_int_employee_skill_master_id        in number default null,
        --p_employee_id                  in number default null,
        p_rating                       in number default null,
        p_experience                   in number default null,
        p_skill_set_id                 in number default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'update_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_int_employee_skill_master_id - ', p_int_employee_skill_master_id);
        --logger.append_param(l_params, 'p_employee_id - ', p_employee_id);
        logger.append_param(l_params, 'p_rating - ', p_rating);
        logger.append_param(l_params, 'p_experience - ', p_experience);
        logger.append_param(l_params, 'p_skill_set_id - ', p_skill_set_id);
        logger.log('START', l_scope, null, l_params);
        update  int_employee_skill_master set 
            rating = p_rating,
            experience = p_experience,
            skill_set_id = p_skill_set_id
        where  int_employee_skill_master_id = p_int_employee_skill_master_id;
        logger.log_info('Employee Skill Updated', l_scope, 'Employee Skill updated in int_employee_skill_master with int_employee_skill_master_id = ' || p_int_employee_skill_master_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end update_row;

    procedure delete_row (
        p_int_employee_skill_master_id        in number
    )
    is
     l_scope logger_logs.scope%type := gc_scope_prefix || 'delete_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_int_employee_skill_master_id - ', p_int_employee_skill_master_id);
        logger.log('START', l_scope, null, l_params);
        delete from int_employee_skill_master where  int_employee_skill_master_id = p_int_employee_skill_master_id;
        logger.log_info('Employee Skill Deleted', l_scope, 'Employee Skill deleted in int_employee_skill_master with int_employee_skill_master_id = ' || p_int_employee_skill_master_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end delete_row;

end int_employee_skill_master_api;
/

