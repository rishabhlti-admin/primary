create or replace package  body int_client_master_api
is
 
 gc_scope_prefix constant VARCHAR2(31) := lower($$PLSQL_UNIT) || '.';
 
    procedure get_row (
        p_id                           in number,
        P_client_name                  out varchar2,
        P_address                      out varchar2,
        P_hq_location                  out varchar2,
        P_contact                      out number
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'get_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_id - ', p_id);
        logger.append_param(l_params, 'P_client_name - ', P_client_name);
        logger.append_param(l_params, 'P_address - ', P_address);
        logger.append_param(l_params, 'P_hq_location - ', P_hq_location);
        logger.append_param(l_params, 'P_contact - ', P_contact);
        for c1 in (select * from int_client_master where int_client_master_id = p_id) loop
            p_client_name := c1.client_name;
            p_address := c1.address;
            p_hq_location := c1.hq_location;
            p_contact := c1.contact;
        end loop;
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end get_row;

 
    procedure insert_row  (
        p_id                           in number default null,
        p_client_name                  in varchar2 default null,
        p_address                      in varchar2 default null,
        p_hq_location                  in varchar2 default null,
        p_contact                      in number default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'insert_row';
    l_params logger.tab_param;
    v_id int_client_master.int_client_master_id%type;
    begin
        logger.append_param(l_params, 'p_id - ', p_id);
        logger.append_param(l_params, 'P_client_name - ', P_client_name);
        logger.append_param(l_params, 'P_address - ', P_address);
        logger.append_param(l_params, 'P_hq_location - ', P_hq_location);
        logger.append_param(l_params, 'P_contact - ', P_contact);
        logger.log('START', l_scope, null, l_params);
        insert into int_client_master (
            int_client_master_id,
            client_name,
            address,
            hq_location,
            contact
        ) values (
            p_id,
            p_client_name,
            p_address,
            p_hq_location,
            p_contact
        ) returning int_client_master_id into v_id;
        logger.log_info('Client Created', l_scope, 'Client created in int_client_master with int_client_master_id = ' || v_id, l_params);
        logger.log('END', l_scope);
        exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end insert_row;

    procedure update_row  (
        p_id                           in number default null,
        p_client_name                  in varchar2 default null,
        p_address                      in varchar2 default null,
        p_hq_location                  in varchar2 default null,
        p_contact                      in number default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'update_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_id - ', p_id);
        logger.append_param(l_params, 'P_client_name - ', P_client_name);
        logger.append_param(l_params, 'P_address - ', P_address);
        logger.append_param(l_params, 'P_hq_location - ', P_hq_location);
        logger.append_param(l_params, 'P_contact - ', P_contact);
        logger.log('START', l_scope, null, l_params);
        update  int_client_master set 
            int_client_master_id = p_id,
            client_name = p_client_name,
            address = p_address,
            hq_location = p_hq_location,
            contact = p_contact
        where int_client_master_id = p_id;
        logger.log_info('Client Updated', l_scope, 'Client updated in int_client_master with int_client_master_id = ' || p_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end update_row;

    procedure delete_row (
        p_id                           in number
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'delete_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_id - ', p_id);
        logger.log('START', l_scope, null, l_params);
        delete from int_client_master where int_client_master_id = p_id;
        logger.log_info('Client Deleted', l_scope, 'Client deleted in int_client_master with int_client_master_id = ' || p_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end delete_row;

end int_client_master_api;
/