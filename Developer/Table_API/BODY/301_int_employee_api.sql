create or replace package  body int_employee_api
is
    gc_scope_prefix constant VARCHAR2(31) := lower($$PLSQL_UNIT) || '.';
    
    procedure get_row (
        p_int_employee_id              in  number,
        p_psno                         out integer,
        p_first_name                   out varchar2,
        p_last_name                    out varchar2,
        p_mobno                        out integer,
        p_email_id                     out varchar2,
        p_designation                  out varchar2,
        p_total_experience             out number,
        p_status                       out varchar2,
        p_reporting_manager            out varchar2,
        p_location                     out varchar2,
        p_cadre                        out varchar2,
        p_practice_id                  out number
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'get_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_int_employee_id - ', p_int_employee_id);
        logger.append_param(l_params, 'p_psno - ', p_psno);
        logger.append_param(l_params, 'p_first_name - ', p_first_name);
        logger.append_param(l_params, 'p_last_name - ', p_last_name);
        logger.append_param(l_params, 'p_mobno - ', p_mobno);
        logger.append_param(l_params, 'p_email_id - ', p_email_id);
        logger.append_param(l_params, 'p_designation - ', p_designation);
        logger.append_param(l_params, 'p_total_experience - ', p_total_experience);
        logger.append_param(l_params, 'p_status - ', p_status);
        logger.append_param(l_params, 'p_reporting_manager - ', p_reporting_manager);
        logger.append_param(l_params, 'p_location - ', p_location);
        logger.append_param(l_params, 'p_cadre - ', p_cadre);
        logger.append_param(l_params, 'p_practice_id - ', p_practice_id);
        for c1 in (select * from int_employee where int_employee_id = p_int_employee_id) loop
            p_psno := c1.psno;
            p_first_name := c1.first_name;
            p_last_name := c1.last_name;
            p_mobno := c1.mobno;
            p_email_id := c1.email_id;
            p_designation := c1.designation;
            p_total_experience := c1.total_experience;
            p_status := c1.status;
            p_reporting_manager := c1.reporting_manager;
            p_location := c1.location;
            p_cadre := c1.cadre;
            p_practice_id := c1.practice_id;
        end loop;
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end get_row;

 
    procedure insert_row  (
        p_int_employee_id              in number default null,
        p_psno                         in integer default null,
        p_first_name                   in varchar2 default null,
        p_last_name                    in varchar2 default null,
        p_mobno                        in integer default null,
        p_email_id                     in varchar2 default null,
        p_designation                  in varchar2 default null,
        p_total_experience             in number default null,
        p_status                       in varchar2 default null,
        p_reporting_manager            in varchar2 default null,
        p_location                     in varchar2 default null,
        p_cadre                        in varchar2 default null,
        p_practice_id                  in number default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'insert_row';
    l_params logger.tab_param;
    v_id int_employee.int_employee_id%type;
    begin
        logger.append_param(l_params, 'p_int_employee_id - ', p_int_employee_id);
        logger.append_param(l_params, 'p_psno - ', p_psno);
        logger.append_param(l_params, 'p_first_name - ', p_first_name);
        logger.append_param(l_params, 'p_last_name - ', p_last_name);
        logger.append_param(l_params, 'p_mobno - ', p_mobno);
        logger.append_param(l_params, 'p_email_id - ', p_email_id);
        logger.append_param(l_params, 'p_designation - ', p_designation);
        logger.append_param(l_params, 'p_total_experience - ', p_total_experience);
        logger.append_param(l_params, 'p_status - ', p_status);
        logger.append_param(l_params, 'p_reporting_manager - ', p_reporting_manager);
        logger.append_param(l_params, 'p_location - ', p_location);
        logger.append_param(l_params, 'p_cadre - ', p_cadre);
        logger.append_param(l_params, 'p_practice_id - ', p_practice_id);
        logger.log('START', l_scope, null, l_params);
        insert into int_employee (
            int_employee_id,
            psno,
            first_name,
            last_name,
            mobno,
            email_id,
            designation,
            total_experience,
            status,
            reporting_manager,
            location,
            cadre,
            practice_id
        ) values (
            p_int_employee_id,
            p_psno,
            p_first_name,
            p_last_name,
            p_mobno,
            p_email_id,
            p_designation,
            p_total_experience,
            p_status,
            p_reporting_manager,
            p_location,
            p_cadre,
            p_practice_id
        )returning int_employee_id into v_id;
        logger.log_info('Employee inserted', l_scope, 'Employee inserted in int_employee with int_employee_id = ' || v_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end insert_row;

    procedure update_row  (
        p_int_employee_id              in number default null,
        p_psno                         in integer default null,
        p_first_name                   in varchar2 default null,
        p_last_name                    in varchar2 default null,
        p_mobno                        in integer default null,
        p_email_id                     in varchar2 default null,
        p_designation                  in varchar2 default null,
        p_total_experience             in number default null,
        p_status                       in varchar2 default null,
        p_reporting_manager            in varchar2 default null,
        p_location                     in varchar2 default null,
        p_cadre                        in varchar2 default null,
        p_practice_id                  in number default null
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'update_row';
    l_params logger.tab_param;
    begin
        logger.append_param(l_params, 'p_int_employee_id - ', p_int_employee_id);
        logger.append_param(l_params, 'p_psno - ', p_psno);
        logger.append_param(l_params, 'p_first_name - ', p_first_name);
        logger.append_param(l_params, 'p_last_name - ', p_last_name);
        logger.append_param(l_params, 'p_mobno - ', p_mobno);
        logger.append_param(l_params, 'p_email_id - ', p_email_id);
        logger.append_param(l_params, 'p_designation - ', p_designation);
        logger.append_param(l_params, 'p_total_experience - ', p_total_experience);
        logger.append_param(l_params, 'p_status - ', p_status);
        logger.append_param(l_params, 'p_reporting_manager - ', p_reporting_manager);
        logger.append_param(l_params, 'p_location - ', p_location);
        logger.append_param(l_params, 'p_cadre - ', p_cadre);
        logger.append_param(l_params, 'p_practice_id - ', p_practice_id);
        logger.log('START', l_scope, null, l_params);
        update  int_employee set 
            int_employee_id = p_int_employee_id,
            psno = p_psno,
            first_name = p_first_name,
            last_name = p_last_name,
            mobno = p_mobno,
            email_id = p_email_id,
            designation = p_designation,
            total_experience = p_total_experience,
            status = p_status,
            reporting_manager = p_reporting_manager,
            location = p_location,
            cadre = p_cadre,
            practice_id = p_practice_id
        where int_employee_id = p_int_employee_id;
        logger.log_info('Employee Updated', l_scope, 'Employee updated in int_employee with int_employee_id = ' || p_int_employee_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end update_row;

    procedure delete_row (
        p_int_employee_id              in number
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'delete_row';
    l_params logger.tab_param;
    begin
    logger.append_param(l_params, 'p_int_employee_id - ', p_int_employee_id);
        logger.log('START', l_scope, null, l_params);
        delete from int_employee where int_employee_id = p_int_employee_id;
        logger.log_info('Emploloyee Deleted', l_scope, 'Employee deleted in int_employee with int_employee_id = ' || p_int_employee_id, l_params);
        logger.log('END', l_scope);
    exception
    when others then
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      raise;
    end delete_row;

end int_employee_api;
/