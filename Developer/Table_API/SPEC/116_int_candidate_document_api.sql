create or replace package int_candidate_document_api
is
 
    /* example:
        declare
            l_candidate_id                  number;
            l_document_name                 varchar2(40);
            l_document                      blob;
            l_last_updated                  date;
        begin
        int_candidate_document_api.get_row (
            p_int_candidate_document_id         => 1,
            p_candidate_id                      => l_candidate_id,
            p_document_name                     => l_document_name,
            p_document                          => l_document,
            p_last_updated                      => l_last_updated
            );
        end;
    */

    procedure get_row (
        p_int_candidate_document_id        in  number,
        p_candidate_id                     out number,
        p_document_name                    out varchar2,
        p_document                         out blob,
        p_last_updated                     out date
    );
 
    /* example:
        begin
        int_candidate_document_api.insert_row (
            p_int_candidate_document_id       => null,
            p_candidate_id               	    => null,
            p_document_name              	    => null,
            p_document                  	    => null,
            p_last_updated             	    => null
            );
        end;
    */

    procedure insert_row  (
        p_int_candidate_document_id        in number default null,
        p_candidate_id                     in number default null,
        p_document_name               	 in varchar2 default null,
        p_document                         in blob default null,
        p_last_updated                	 in date default null
    );
    procedure update_row  (
        p_int_candidate_document_id        in number default null,
        p_candidate_id                     in number default null,
        p_document_name                    in varchar2 default null,
        p_document                         in blob default null,
        p_last_updated                     in date default null
    );
    procedure delete_row (
        p_int_candidate_document_id        in number
    );
end int_candidate_document_api;
/
