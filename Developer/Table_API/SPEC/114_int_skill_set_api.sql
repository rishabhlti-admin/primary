create or replace package int_skill_set_api
is
 
    /* example:
        declare
            l_role_id                       number;
            l_skill_name                    varchar2(20);
        begin
        int_skill_set_api.get_row (
            p_int_skill_set_id              => 1,
            p_role_id                       => l_role_id,
            p_skill_name                    => l_skill_name
            );
        end;
    */

    procedure get_row (
        p_int_skill_set_id             in  number,
        p_role_id                      out number,
        p_skill_name                   out varchar2
    );
 
    /* example:
        begin
        int_skill_set_api.insert_row (
            p_int_skill_set_id            => null,
            p_role_id                     => null,
            p_skill_name                  => null
            );
        end;
    */

    procedure insert_row  (
        p_int_skill_set_id             in number default null,
        p_role_id                      in number default null,
        p_skill_name                   in varchar2 default null
    );
    procedure update_row  (
        p_int_skill_set_id             in number default null,
        p_role_id                      in number default null,
        p_skill_name                   in varchar2 default null
    );
    procedure delete_row (
        p_int_skill_set_id             in number
    );
end int_skill_set_api;
/
