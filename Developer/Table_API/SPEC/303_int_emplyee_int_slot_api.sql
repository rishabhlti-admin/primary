create or replace package int_employee_int_slot_api
is
 
    /* example:
        declare
            l_employee_id                   number;
            l_interview_dt                  date;
            l_start_time                    timestamp;
            l_end_time                      timestamp;
            l_status                        varchar2(30);
        begin
        int_employee_int_slot_api.get_row (
            p_int_employee_int_slot_id      => 1,
            p_employee_id                   => l_employee_id,
            p_interview_dt                  => l_interview_dt,
            p_start_time                    => l_start_time,
            p_end_time                      => l_end_time,
            p_status                        => l_status
            );
        end;
    */

    procedure get_row (
        p_int_employee_int_slot_id     in  number,
        p_employee_id                  out number,
        p_interview_dt                 out date,
        p_start_time                   out timestamp,
        p_end_time                     out timestamp,
        p_status                       out varchar2
    );
 
    /* example:
        begin
        int_employee_int_slot_api.insert_row (
            p_int_employee_int_slot_id    => null,
            p_employee_id                 => null,
            p_interview_dt                => null,
            p_start_time                  => null,
            p_end_time                    => null,
            p_status                      => null
            );
        end;
    */

    procedure insert_row  (
        p_int_employee_int_slot_id     in number default null,
        p_employee_id                  in number default null,
        p_interview_dt                 in date default null,
        p_start_time                   in timestamp default null,
        p_end_time                     in timestamp default null,
        p_status                       in varchar2 default null
    );
    procedure update_row  (
        p_int_employee_int_slot_id     in number default null,
        p_employee_id                  in number default null,
        p_interview_dt                 in date default null,
        p_start_time                   in timestamp default null,
        p_end_time                     in timestamp default null,
        p_status                       in varchar2 default null
    );
    procedure delete_row (
        p_int_employee_int_slot_id     in number
    );
end int_employee_int_slot_api;
/