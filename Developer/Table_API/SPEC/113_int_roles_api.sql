

create or replace package int_roles_api
is
 
    /* example:
        declare
            l_rolename                      varchar2(30);
        begin
        int_roles_api.get_row (
            p_int_role_id                   => 1,
            p_rolename                      => l_rolename
            );
        end;
    */

    procedure get_row (
        p_int_role_id                  in  number,
        p_rolename                     out varchar2
    );
 
    /* example:
        begin
        int_roles_api.insert_row (
            p_int_role_id                 => null,
            p_rolename                    => null
            );
        end;
    */

    procedure insert_row  (
        p_int_role_id                  in number default null,
        p_rolename                     in varchar2 default null
    );
    procedure update_row  (
        p_int_role_id                  in number default null,
        p_rolename                     in varchar2 default null
    );
    procedure delete_row (
        p_int_role_id                  in number
    );
end int_roles_api;
/