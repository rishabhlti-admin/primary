
create or replace package int_employee_api
is
 
    /* example:
        declare
            l_psno                          integer;
            l_first_name                    varchar2(30);
            l_last_name                     varchar2(30);
            l_mobno                         integer;
            l_email_id                      varchar2(40);
            l_designation                   varchar2(50);
            l_total_experience              number;
            l_status                        varchar2(10);
            l_reporting_manager             varchar2(40);
            l_location                      varchar2(30);
            l_cadre                         varchar2(10);
            l_practice_id                   number;
        begin
        int_employee_api.get_row (
            p_int_employee_id               => 1,
            p_psno                          => l_psno,
            p_first_name                    => l_first_name,
            p_last_name                     => l_last_name,
            p_mobno                         => l_mobno,
            p_email_id                      => l_email_id,
            p_designation                   => l_designation,
            p_total_experience              => l_total_experience,
            p_status                        => l_status,
            p_reporting_manager             => l_reporting_manager,
            p_location                      => l_location,
            p_cadre                         => l_cadre,
            p_practice_id                   => l_practice_id
            );
        end;
    */

    procedure get_row (
        p_int_employee_id              in  number,
        p_psno                         out integer,
        p_first_name                   out varchar2,
        p_last_name                    out varchar2,
        p_mobno                        out integer,
        p_email_id                     out varchar2,
        p_designation                  out varchar2,
        p_total_experience             out number,
        p_status                       out varchar2,
        p_reporting_manager            out varchar2,
        p_location                     out varchar2,
        p_cadre                        out varchar2,
        p_practice_id                  out number
    );
 
    /* example:
        begin
        int_employee_api.insert_row (
            p_int_employee_id             => null,
            p_psno                        => null,
            p_first_name                  => null,
            p_last_name                   => null,
            p_mobno                       => null,
            p_email_id                    => null,
            p_designation                 => null,
            p_total_experience            => null,
            p_status                      => null,
            p_reporting_manager           => null,
            p_location                    => null,
            p_cadre                       => null,
            p_practice_id                 => null
            );
        end;
    */

    procedure insert_row  (
        p_int_employee_id              in number default null,
        p_psno                         in integer default null,
        p_first_name                   in varchar2 default null,
        p_last_name                    in varchar2 default null,
        p_mobno                        in integer default null,
        p_email_id                     in varchar2 default null,
        p_designation                  in varchar2 default null,
        p_total_experience             in number default null,
        p_status                       in varchar2 default null,
        p_reporting_manager            in varchar2 default null,
        p_location                     in varchar2 default null,
        p_cadre                        in varchar2 default null,
        p_practice_id                  in number default null
    );
    procedure update_row  (
        p_int_employee_id              in number default null,
        p_psno                         in integer default null,
        p_first_name                   in varchar2 default null,
        p_last_name                    in varchar2 default null,
        p_mobno                        in integer default null,
        p_email_id                     in varchar2 default null,
        p_designation                  in varchar2 default null,
        p_total_experience             in number default null,
        p_status                       in varchar2 default null,
        p_reporting_manager            in varchar2 default null,
        p_location                     in varchar2 default null,
        p_cadre                        in varchar2 default null,
        p_practice_id                  in number default null
    );
    procedure delete_row (
        p_int_employee_id              in number
    );
end int_employee_api;
/