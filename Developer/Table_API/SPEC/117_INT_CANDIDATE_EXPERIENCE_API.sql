create or replace package int_candidate_experience_api
is
 
    /* example:
        declare
            l_candidate_id                  number;
            l_experience                    number;
            l_company_name                  varchar2(100);
            l_start_date                    date;
            l_end_date                      date;
        begin
        int_candidate_experience_api.get_row (
            p_int_candidate_experience_id         => 1,
            p_candidate_id                  	  => l_candidate_id,
            p_experience                   	  => l_experience,
            p_company_name                   	  => l_company_name,
            p_start_date                          => l_start_date,
            p_end_date                            => l_end_date
            );
        end;
    */

    procedure get_row (
        p_int_candidate_experience_id        in  number,
        p_candidate_id                       out number,
        p_experience                         out number,
        p_company_name                       out varchar2,
        p_start_date                         out date,
        p_end_date                           out date
    );
 
    /* example:
        begin
        int_candidate_experience_api.insert_row (
            p_int_candidate_experience_id       => null,
            p_candidate_id                      => null,
            p_experience                        => null,
            p_company_name                      => null,
            p_start_date                        => null,
            p_end_date                          => null
            );
        end;
    */

    procedure insert_row  (
        p_int_candidate_experience_id        in number default null,
        p_candidate_id                       in number default null,
        p_experience                         in number default null,
        p_company_name                       in varchar2 default null,
        p_start_date                         in date default null,
        p_end_date                           in date default null
    );
    procedure update_row  (
        p_int_candidate_experience_id        in number default null,
        p_candidate_id                       in number default null,
        p_experience                         in number default null,
        p_company_name                       in varchar2 default null,
        p_start_date                         in date default null,
        p_end_date                           in date default null
    );
    procedure delete_row (
        p_int_candidate_experience_id        in number
    );
end int_candidate_experience_api;
/
