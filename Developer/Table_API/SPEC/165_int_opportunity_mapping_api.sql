create or replace package int_opportunity_mapping_api
is
 
    /* example:
        declare
            l_int_opportunity_breakup_id    number;
            l_experience_in_specified_skill number;
            l_int_skill_set_id              number;
        begin
        int_opportunity_mapping_api.get_row (
            p_id                            => 1,
            p_int_opportunity_breakup_id    => l_int_opportunity_breakup_id,
            p_experience_in_specified_skill => l_experience_in_specified_skill,
            p_int_skill_set_id              => l_int_skill_set_id
            );
        end;
    */

    procedure get_row (
        p_id                           in number,
        P_int_opportunity_breakup_id   out number,
        P_experience_in_specified_skill out number,
        P_int_skill_set_id             out number
    );

    /* example:
        begin
        int_opportunity_mapping_api.insert_row (
            p_id                          => null,
            p_int_opportunity_breakup_id  => null,
            p_experience_in_specified_skil=> null,
            p_int_skill_set_id            => null
            );
        end;
    */

    procedure insert_row  (
        p_id                           in number default null,
        p_int_opportunity_breakup_id   in number default null,
        p_experience_in_specified_skill in number default null,
        p_int_skill_set_id             in number default null
    );
    procedure update_row  (
        p_id                           in number default null,
        p_int_opportunity_breakup_id   in number default null,
        p_experience_in_specified_skill in number default null,
        p_int_skill_set_id             in number default null
    );
    procedure delete_row (
        p_id                           in number
    );
end int_opportunity_mapping_api;


