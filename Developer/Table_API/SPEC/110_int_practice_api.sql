create or replace package int_practice_api AUTHID current_user AS

-- =============================================
-- Author:      Rishabh Chowdhury
-- Create date: 26-Jan-2022
-- Description: int_practice_api is packages containing crud   
--              api for int_practice.
-- =============================================
 
    /* example:
        declare
            l_int_business_unit_id          number;
            l_practice_name                 varchar2(100);
            l_practice_head                 varchar2(100);
            l_head_email                    varchar2(100);
        begin
        int_practice_api.get_row (
            p_id                            => 1,
            p_int_business_unit_id          => l_int_business_unit_id,
            p_practice_name                 => l_practice_name,
            p_practice_head                 => l_practice_head,
            p_head_email                    => l_head_email
            );
        end;
    */

    procedure get_row (
        p_id                           in number,
        P_int_business_unit_id         out number,
        P_practice_name                out varchar2,
        P_practice_head                out varchar2,
        P_head_email                   out varchar2
    );
 
    /* example:
        begin
        int_practice_api.insert_row (
            p_id                          => null,
            p_int_business_unit_id        => null,
            p_practice_name               => null,
            p_practice_head               => null,
            p_head_email                  => null
            );
        end;
    */

    procedure insert_row  (
        p_id                           in number default null,
        p_int_business_unit_id         in number default null,
        p_practice_name                in varchar2 default null,
        p_practice_head                in varchar2 default null,
        p_head_email                   in varchar2 default null
    );
    procedure update_row  (
        p_id                           in number default null,
        p_int_business_unit_id         in number default null,
        p_practice_name                in varchar2 default null,
        p_practice_head                in varchar2 default null,
        p_head_email                   in varchar2 default null
    );
    procedure delete_row (
        p_id                           in number
    );
end int_practice_api;
/