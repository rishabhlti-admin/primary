create or replace package int_business_unit_api
is
 
    /* example:
        declare
            l_bu_name                       varchar2(255);
            l_bu_head                       varchar2(4000);
        begin
        int_business_unit_api.get_row (
            p_id                            => 1,
            p_bu_name                       => l_bu_name,
            p_bu_head                       => l_bu_head
            );
        end;
    */

    procedure get_row (
        p_id                           in number,
        P_bu_name                      out varchar2,
        P_bu_head                      out varchar2
    );
 
    /* example:
        begin
        int_business_unit_api.insert_row (
            p_id                          => null,
            p_bu_name                     => null,
            p_bu_head                     => null
            );
        end;
    */

    procedure insert_row  (
        p_id                           in number default null,
        p_bu_name                      in varchar2 default null,
        p_bu_head                      in varchar2 default null
    );
    procedure update_row  (
        p_id                           in number default null,
        p_bu_name                      in varchar2 default null,
        p_bu_head                      in varchar2 default null
    );
    procedure delete_row (
        p_id                           in number
    );
end int_business_unit_api;
/