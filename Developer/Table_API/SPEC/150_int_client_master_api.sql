-- APIs --
create or replace package int_client_master_api
is
 
    /* example:
        declare
            l_client_name                   varchar2(50);
            l_address                       varchar2(200);
            l_hq_location                   varchar2(100);
            l_contact                       number;
        begin
        int_client_master_api.get_row (
            p_id                            => 1,
            p_client_name                   => l_client_name,
            p_address                       => l_address,
            p_hq_location                   => l_hq_location,
            p_contact                       => l_contact
            );
        end;
    */

    procedure get_row (
        p_id                           in number,
        P_client_name                  out varchar2,
        P_address                      out varchar2,
        P_hq_location                  out varchar2,
        P_contact                      out number
    );
 
    /* example:
        begin
        int_client_master_api.insert_row (
            p_id                          => null,
            p_client_name                 => null,
            p_address                     => null,
            p_hq_location                 => null,
            p_contact                     => null
            );
        end;
    */

    procedure insert_row  (
        p_id                           in number default null,
        p_client_name                  in varchar2 default null,
        p_address                      in varchar2 default null,
        p_hq_location                  in varchar2 default null,
        p_contact                      in number default null
    );
    procedure update_row  (
        p_id                           in number default null,
        p_client_name                  in varchar2 default null,
        p_address                      in varchar2 default null,
        p_hq_location                  in varchar2 default null,
        p_contact                      in number default null
    );
    procedure delete_row (
        p_id                           in number
    );
end int_client_master_api;
/
