-- APIs --
create or replace package int_opportunity_master_api
is
 
    /* example:
        declare
            l_project_name                  varchar2(50);
            l_project_desc                  varchar2(500);
            l_no_of_positions               number;
            l_start_date                    date;
            l_end_date                      date;
            l_int_client_master_id          number;
            l_fp_tbm                        varchar2(20);
            l_spoc                          varchar2(20);
        begin
        int_opportunity_master_api.get_row (
            p_id                            => 1,
            p_project_name                  => l_project_name,
            p_project_desc                  => l_project_desc,
            p_no_of_positions               => l_no_of_positions,
            p_start_date                    => l_start_date,
            p_end_date                      => l_end_date,
            p_int_client_master_id          => l_int_client_master_id,
            p_fp_tbm                        => l_fp_tbm,
            p_spoc                          => l_spoc
            );
        end;
    */

    procedure get_row (
        p_id                           in number,
        P_project_name                 out varchar2,
        P_project_desc                 out varchar2,
        P_no_of_positions              out number,
        P_start_date                   out date,
        P_end_date                     out date,
        P_int_client_master_id         out number,
        P_fp_tbm                       out varchar2,
        P_spoc                         out varchar2
    );
 
    /* example:
        begin
        int_opportunity_master_api.insert_row (
            p_id                          => null,
            p_project_name                => null,
            p_project_desc                => null,
            p_no_of_positions             => null,
            p_start_date                  => null,
            p_end_date                    => null,
            p_int_client_master_id        => null,
            p_fp_tbm                      => null,
            p_spoc                        => null
            );
        end;
    */

    procedure insert_row  (
        p_id                           in number default null,
        p_project_name                 in varchar2 default null,
        p_project_desc                 in varchar2 default null,
        p_no_of_positions              in number default null,
        p_start_date                   in date default null,
        p_end_date                     in date default null,
        p_int_client_master_id         in number default null,
        p_fp_tbm                       in varchar2 default null,
        p_spoc                         in varchar2 default null
    );
    procedure update_row  (
        p_id                           in number default null,
        p_project_name                 in varchar2 default null,
        p_project_desc                 in varchar2 default null,
        p_no_of_positions              in number default null,
        p_start_date                   in date default null,
        p_end_date                     in date default null,
        p_int_client_master_id         in number default null,
        p_fp_tbm                       in varchar2 default null,
        p_spoc                         in varchar2 default null
    );
    procedure delete_row (
        p_id                           in number
    );
end int_opportunity_master_api;
/
