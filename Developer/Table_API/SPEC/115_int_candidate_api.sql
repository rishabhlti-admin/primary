create or replace package int_candidate_api
is
 
    /* example:
        declare
            l_first_name                    varchar2(40);
            l_last_name                     varchar2(40);
            l_date_of_birth                 date;
            l_emailid                       varchar2(100);
            l_gender                        varchar2(20);
            l_phone_number                  number;
            l_city                          varchar2(40);
            l_zip_code                      number;
            l_state                         varchar2(40);
            l_country                       varchar2(40);
            l_nationality                   varchar2(40);
            l_primary_education             number;
            l_secondary_education           number;
            l_graduation                    number;
            l_post_graduation               number;
            l_total_experience              number;
        begin
        int_candidate_api.get_row (
            p_int_candidate_id              => 1,
            p_first_name                    => l_first_name,
            p_last_name                     => l_last_name,
            p_date_of_birth                 => l_date_of_birth,
            p_emailid                       => l_emailid,
            p_gender                        => l_gender,
            p_phone_number                  => l_phone_number,
            p_city                          => l_city,
            p_zip_code                      => l_zip_code,
            p_state                         => l_state,
            p_country                       => l_country,
            p_nationality                   => l_nationality,
            p_primary_education             => l_primary_education,
            p_secondary_education           => l_secondary_education,
            p_graduation                    => l_graduation,
            p_post_graduation               => l_post_graduation,
            p_total_experience              => l_total_experience
            );
        end;
    */

    procedure get_row (
        p_int_candidate_id             in  number,
        p_first_name                   out varchar2,
        p_last_name                    out varchar2,
        p_date_of_birth                out date,
        p_emailid                      out varchar2,
        p_gender                       out varchar2,
        p_phone_number                 out number,
        p_city                         out varchar2,
        p_zip_code                     out number,
        p_state                        out varchar2,
        p_country                      out varchar2,
        p_nationality                  out varchar2,
        p_primary_education            out number,
        p_secondary_education          out number,
        p_graduation                   out number,
        p_post_graduation              out number,
        p_total_experience             out number
    );
 
    /* example:
        begin
        int_candidate_api.insert_row (
            p_int_candidate_id            => null,
            p_first_name                  => null,
            p_last_name                   => null,
            p_date_of_birth               => null,
            p_emailid                     => null,
            p_gender                      => null,
            p_phone_number                => null,
            p_city                        => null,
            p_zip_code                    => null,
            p_state                       => null,
            p_country                     => null,
            p_nationality                 => null,
            p_primary_education           => null,
            p_secondary_education         => null,
            p_graduation                  => null,
            p_post_graduation             => null,
            p_total_experience            => null
            );
        end;
    */

    procedure insert_row  (
        p_int_candidate_id             in number default null,
        p_first_name                   in varchar2 default null,
        p_last_name                    in varchar2 default null,
        p_date_of_birth                in date default null,
        p_emailid                      in varchar2 default null,
        p_gender                       in varchar2 default null,
        p_phone_number                 in number default null,
        p_city                         in varchar2 default null,
        p_zip_code                     in number default null,
        p_state                        in varchar2 default null,
        p_country                      in varchar2 default null,
        p_nationality                  in varchar2 default null,
        p_primary_education            in number default null,
        p_secondary_education          in number default null,
        p_graduation                   in number default null,
        p_post_graduation              in number default null,
        p_total_experience             in number default null
    );
    procedure update_row  (
        p_int_candidate_id             in number default null,
        p_first_name                   in varchar2 default null,
        p_last_name                    in varchar2 default null,
        p_date_of_birth                in date default null,
        p_emailid                      in varchar2 default null,
        p_gender                       in varchar2 default null,
        p_phone_number                 in number default null,
        p_city                         in varchar2 default null,
        p_zip_code                     in number default null,
        p_state                        in varchar2 default null,
        p_country                      in varchar2 default null,
        p_nationality                  in varchar2 default null,
        p_primary_education            in number default null,
        p_secondary_education          in number default null,
        p_graduation                   in number default null,
        p_post_graduation              in number default null,
        p_total_experience             in number default null
    );
    procedure delete_row (
        p_int_candidate_id             in number
    );
end int_candidate_api;
/
