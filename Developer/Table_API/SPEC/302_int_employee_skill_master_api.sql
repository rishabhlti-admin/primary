create or replace package int_employee_skill_master_api
is
 
    /* example:
        declare
            l_employee_id                   number;
            l_rating                        number;
            l_experience                    number;
            l_skill_set_id                  number;
        begin
        int_employee_skill_master_api.get_row (
            p_int_employee_skill_id         => 1,
            p_employee_id                   => l_employee_id,
            p_rating                        => l_rating,
            p_experience                    => l_experience,
            p_skill_set_id                  => l_skill_set_id
            );
        end;
    */

    procedure get_row (
        p_int_employee_skill_master_id        in  number,
        p_employee_id                  out number,
        p_rating                       out number,
        p_experience                   out number,
        p_skill_set_id                 out number
    );
 
    /* example:
        begin
        int_employee_skill_master_api.insert_row (
            p_int_employee_skill_id       => null,
            p_employee_id                 => null,
            p_rating                      => null,
            p_experience                  => null,
            p_skill_set_id                => null
            );
        end;
    */

    procedure insert_row  (
        p_int_employee_skill_master_id        in number default null,
        p_employee_id                  in number default null,
        p_rating                       in number default null,
        p_experience                   in number default null,
        p_skill_set_id                 in number default null
    );
    procedure update_row  (
        p_int_employee_skill_master_id        in number default null,
        --p_employee_id                  in number default null,
        p_rating                       in number default null,
        p_experience                   in number default null,
        p_skill_set_id                 in number default null
    );
    procedure delete_row (
        p_int_employee_skill_master_id        in number
    );
end int_employee_skill_master_api;
/
