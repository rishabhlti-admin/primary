-- APIs --
create or replace package int_opportunity_breakup_api
is
 
    /* example:
        declare
            l_int_opportunity_master_id     number;
            l_experience                    number;
            l_cadre                         varchar2(5);
            l_location                      varchar2(100);
            l_int_practice_id               number;
        begin
        int_opportunity_breakup_api.get_row (
            p_id                            => 1,
            p_int_opportunity_master_id     => l_int_opportunity_master_id,
            p_experience                    => l_experience,
            p_cadre                         => l_cadre,
            p_location                      => l_location,
            p_int_practice_id               => l_int_practice_id
            );
        end;
    */

    procedure get_row (
        p_id                           in number,
        P_int_opportunity_master_id    out number,
        P_experience                   out number,
        P_cadre                        out varchar2,
        P_location                     out varchar2,
        P_int_practice_id              out number
    );
 
    /* example:
        begin
        int_opportunity_breakup_api.insert_row (
            p_id                          => null,
            p_int_opportunity_master_id   => null,
            p_experience                  => null,
            p_cadre                       => null,
            p_location                    => null,
            p_int_practice_id             => null
            );
        end;
    */

    procedure insert_row  (
        p_id                           in number default null,
        p_int_opportunity_master_id    in number default null,
        p_experience                   in number default null,
        p_cadre                        in varchar2 default null,
        p_location                     in varchar2 default null,
        p_int_practice_id              in number default null
    );
    procedure update_row  (
        p_id                           in number default null,
        p_int_opportunity_master_id    in number default null,
        p_experience                   in number default null,
        p_cadre                        in varchar2 default null,
        p_location                     in varchar2 default null,
        p_int_practice_id              in number default null
    );
    procedure delete_row (
        p_id                           in number
    );
end int_opportunity_breakup_api;
/
