CREATE OR REPLACE TYPE body user_t AS

-- =============================================
-- Author:      Rishabh Chowdhury
-- Create date: 18-Apr-2022
-- Description: user_t is child of person object  
-- =============================================
    
    OVERRIDING member FUNCTION obj2string RETURN VARCHAR2
    IS
    BEGIN
        RETURN 
        (   
                'username'                  || self.username
             || 'access_privs - '           || self.access_privs 
             || 'firstname - '              || self.firstname 
             || 'lastname - '               || self.lastname
             || 'email_id - '               || self.email_id
             || 'phone - '                  || self.phone
        );
    END;
    
END;
/