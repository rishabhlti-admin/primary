CREATE OR REPLACE TYPE BODY skill_rate_t AS 

-- =============================================
-- Author:      Rishabh Chowdhury
-- Create date: 30-Aug-2022
-- Description: skill_rate_t is skill set of person object  
-- =============================================

    CONSTRUCTOR FUNCTION skill_rate_t RETURN SELF AS RESULT
    IS
    BEGIN
        RETURN;
    END;
    
    CONSTRUCTOR FUNCTION skill_rate_t 
    (
        skill_set_id                   number,
        experience                     number,
        rating                         number
    )
    RETURN SELF AS RESULT
    IS
    BEGIN
        SELF.emp_skill_id           := NULL;
        SELF.skill_set_id           := skill_set_id;
        SELF.experience             := experience;
        SELF.rating                 := rating;
        RETURN;
    END;
    
    CONSTRUCTOR FUNCTION skill_rate_t 
    (
        emp_skill_id                   number,
        skill_set_id                   number,
        experience                     number,
        rating                         number
    )
    RETURN SELF AS RESULT
    IS
    BEGIN
        SELF.emp_skill_id           := emp_skill_id;
        SELF.skill_set_id           := skill_set_id;
        SELF.experience             := experience;
        SELF.rating                 := rating;
        RETURN;
    END;
    
    CONSTRUCTOR FUNCTION skill_rate_t 
    (
        emp_skill_id                   number
    )
    RETURN SELF AS RESULT
    IS
    BEGIN
        SELF.emp_skill_id           := emp_skill_id;
        SELF.skill_set_id           := NULL;
        SELF.experience             := NULL;
        SELF.rating                 := NULL;
        RETURN;
    END;
    
    MEMBER FUNCTION obj2string RETURN VARCHAR2
    is
    begin
        RETURN 
        (  
                'emp_skill_id - '           || self.emp_skill_id
             || 'skill_set_id - '           || self.skill_set_id 
             || 'experience - '             || self.experience
             || 'rating - '                 || self.rating
        );
    end;

END;
/