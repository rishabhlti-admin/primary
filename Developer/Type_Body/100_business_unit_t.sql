CREATE OR REPLACE TYPE BODY business_unit_t AS 

-- =============================================
-- Author:      Rishabh Chowdhury
-- Create date: 26-Jan-2022
-- Description: business_unit_t is business unit object  
-- =============================================

    CONSTRUCTOR FUNCTION business_unit_t RETURN SELF AS RESULT
    IS
    BEGIN
        SELF.int_business_unit_id   := NULL;
        SELF.bu_name                := NULL;
        SELF.bu_head                := NULL;
        RETURN;
    END;
    
    CONSTRUCTOR FUNCTION business_unit_t 
    (
        bu_name                        VARCHAR2,
        bu_head                        VARCHAR2
    )
    RETURN SELF AS RESULT
    IS
    BEGIN
        SELF.int_business_unit_id   := NULL;
        SELF.bu_name                := bu_name;
        SELF.bu_head                := bu_name;
        RETURN;
    END;
    
    CONSTRUCTOR FUNCTION business_unit_t 
    (
        int_business_unit_id           number,
        bu_name                        VARCHAR2,
        bu_head                        VARCHAR2
    )
    RETURN SELF AS RESULT
    IS
    BEGIN
        SELF.int_business_unit_id   := int_business_unit_id;
        SELF.bu_name                := bu_name;
        SELF.bu_head                := bu_name;
        RETURN;
    END;
    
    MEMBER FUNCTION obj2string RETURN VARCHAR2
    is
    begin
        RETURN 
        (   
                'int_business_unit_id - '   || self.int_business_unit_id 
             || 'bu_name - '                || self.bu_name
             || 'bu_head - '                || self.bu_head
        );
    end;

END;
/