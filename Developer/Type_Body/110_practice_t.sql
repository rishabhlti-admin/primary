CREATE OR REPLACE TYPE BODY practice_t AS

-- =============================================
-- Author:      Rishabh Chowdhury
-- Create date: 26-Jan-2022
-- Description: practice_t is practice object  
-- =============================================

    CONSTRUCTOR FUNCTION practice_t RETURN SELF AS RESULT IS
    BEGIN
        self.int_practice_id        := NULL;
        self.int_business_unit_id   := NULL;
        self.practice_name          := NULL;
        self.practice_head          := NULL;
        self.head_email             := NULL;
        RETURN;
    END;
    
    CONSTRUCTOR FUNCTION practice_t 
    (
        int_business_unit_id NUMBER,
        practice_name        VARCHAR2,
        practice_head        VARCHAR2
    )
    RETURN SELF AS RESULT
    IS
    BEGIN
        self.int_practice_id        := NULL;
        self.int_business_unit_id   := int_business_unit_id;
        self.practice_name          := practice_name;
        self.practice_head          := practice_head;
        self.head_email             := NULL;
        RETURN;
    END;
    
    CONSTRUCTOR FUNCTION practice_t 
    (
        int_business_unit_id NUMBER,
        practice_name        VARCHAR2,
        practice_head        VARCHAR2,
        head_email           VARCHAR2
    )
    RETURN SELF AS RESULT
    IS
    BEGIN
        self.int_practice_id        := NULL;
        self.int_business_unit_id   := int_business_unit_id;
        self.practice_name          := practice_name;
        self.practice_head          := practice_head;
        self.head_email             := head_email;
        RETURN;
    END;
    
    CONSTRUCTOR FUNCTION practice_t 
    (
        int_practice_id      NUMBER,
        int_business_unit_id NUMBER,
        practice_name        VARCHAR2,
        practice_head        VARCHAR2,
        head_email           VARCHAR2
    )
    RETURN SELF AS RESULT
    IS
    BEGIN
        self.int_practice_id        := int_practice_id;
        self.int_business_unit_id   := int_business_unit_id;
        self.practice_name          := practice_name;
        self.practice_head          := practice_head;
        self.head_email             := head_email;
        RETURN;
    END;
    

    MEMBER FUNCTION obj2string RETURN VARCHAR2
    is
    begin
        RETURN 
        (   
                'int_practice_id - '        || self.int_practice_id
             || 'int_business_unit_id - '   || self.int_business_unit_id 
             || 'practice_name - '          || self.int_practice_id
             || 'practice_head - '          || self.int_practice_id
             || 'head_email - '             || self.int_practice_id
        );
    end;
    
END;
/