CREATE OR REPLACE TYPE body employee_t AS

-- =============================================
-- Author:      Rishabh Chowdhury
-- Create date: 29-Aug-2022
-- Description: employee_t is child of person object  
-- =============================================
    
    CONSTRUCTOR FUNCTION employee_t RETURN SELF AS RESULT IS
    BEGIN
        RETURN;
    END;
    
    CONSTRUCTOR FUNCTION employee_t 
    (
        psno                            NUMBER,
        designation                     VARCHAR2,
        experience                      NUMBER,
        manager                         VARCHAR2,
        location                        VARCHAR2,
        status                          varchar2,
        cadre                           varchar2,
        practice_id                     number
    )
    RETURN SELF AS RESULT
    IS
    BEGIN
        self.emp_id                 := NULL;
        self.psno                   := psno;
        self.designation            := designation;
        self.experience             := experience;
        self.manager                := manager;
        self.location               := location;
        self.status                 := status;
        self.cadre                  := cadre;
        self.practice_id            := practice_id;
        RETURN;
    END;
    
    CONSTRUCTOR FUNCTION employee_t 
    (
        emp_id                          NUMBER,
        psno                            NUMBER,
        designation                     VARCHAR2,
        experience                      NUMBER,
        manager                         VARCHAR2,
        location                        VARCHAR2,
        status                          varchar2,
        cadre                           varchar2,
        practice_id                     number
    )
    RETURN SELF AS RESULT
    IS
    BEGIN
        self.emp_id                 := emp_id;
        self.psno                   := psno;
        self.designation            := designation;
        self.experience             := experience;
        self.manager                := manager;
        self.location               := location;
        self.status                 := status;
        self.cadre                  := cadre;
        self.practice_id            := practice_id;
        RETURN;
    END;
    
    CONSTRUCTOR FUNCTION employee_t 
    (
        emp_id                          NUMBER
    )
    RETURN SELF AS RESULT
    IS
    BEGIN
        self.emp_id                 := emp_id;
        self.psno                   := NULL;
        self.designation            := NULL;
        self.experience             := NULL;
        self.manager                := NULL;
        self.location               := NULL;
        self.status                 := NULL;
        self.cadre                  := NULL;
        self.practice_id            := NULL;
        RETURN;
    END;
    
    OVERRIDING member FUNCTION obj2string RETURN VARCHAR2
    IS
    BEGIN
        RETURN 
        (   
                'psno'                      || self.psno
             || 'designation - '            || self.designation 
             || 'firstname - '              || self.firstname 
             || 'lastname - '               || self.lastname
             || 'email_id - '               || self.email_id
             || 'phone - '                  || self.phone
             || 'experience - '             || self.experience
             || 'manager - '                || self.manager
             || 'location - '               || self.location
             || 'status - '                 || self.status
             || 'cadre - '                  || self.cadre
             || 'practice_id - '            || self.practice_id
        );
    END;
    
END;
/