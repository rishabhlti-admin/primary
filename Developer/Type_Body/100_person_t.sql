CREATE OR REPLACE TYPE body person_t AS

-- =============================================
-- Author:      Rishabh Chowdhury
-- Create date: 30-Jan-2022
-- Description: person_t is parent person object  
-- =============================================

    CONSTRUCTOR FUNCTION person_t RETURN SELF AS RESULT
    IS
    BEGIN
        SELF.firstname              := NULL;
        SELF.lastname               := NULL;
        SELF.email_id               := NULL;
        SELF.phone                  := NULL;
        RETURN;
    END;
    
    CONSTRUCTOR FUNCTION person_t 
    (
        firstname                       VARCHAR2,
        lastname                        VARCHAR2,
        email_id                        VARCHAR2,
        phone                           NUMBER
    )
    RETURN SELF AS RESULT
    IS
    BEGIN
        SELF.firstname              := firstname;
        SELF.lastname               := lastname;
        SELF.email_id               := email_id;
        SELF.phone                  := phone;
        RETURN;
    END;
    
    MEMBER FUNCTION obj2string RETURN VARCHAR2
    IS
    BEGIN
        RETURN 
        (   
                'firstname - '              || self.firstname 
             || 'lastname - '               || self.lastname
             || 'email_id - '               || self.email_id
             || 'phone - '                  || self.phone
        );
    END;
    
END;
/