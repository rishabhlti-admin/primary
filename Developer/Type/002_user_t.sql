CREATE OR REPLACE TYPE user_t under person_t

-- =============================================
-- Author:      Rishabh Chowdhury
-- Create date: 18-Apr-2022
-- Description: user_t is child of person object  
-- =============================================

(
    username                        VARCHAR2(50),
    access_privs                    VARCHAR2(50),
    
    OVERRIDING member FUNCTION obj2string RETURN VARCHAR2
)
NOT FINAL;
/