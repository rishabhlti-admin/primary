CREATE OR REPLACE TYPE business_unit_t AS OBJECT

-- =============================================
-- Author:      Rishabh Chowdhury
-- Create date: 26-Jan-2022
-- Description: business_unit_t is business unit object  
-- =============================================

(
    int_business_unit_id           number,
    bu_name                        VARCHAR2(255 CHAR),
    bu_head                        VARCHAR2(4000 char),
    
    CONSTRUCTOR FUNCTION business_unit_t RETURN SELF AS RESULT,
    
    CONSTRUCTOR FUNCTION business_unit_t 
    (
        bu_name                        VARCHAR2,
        bu_head                        VARCHAR2
    )
    RETURN SELF AS RESULT,
    
    CONSTRUCTOR FUNCTION business_unit_t 
    (
        int_business_unit_id           number,
        bu_name                        VARCHAR2,
        bu_head                        VARCHAR2
    )
    RETURN SELF AS RESULT,
    
    MEMBER FUNCTION obj2string RETURN VARCHAR2
);
/