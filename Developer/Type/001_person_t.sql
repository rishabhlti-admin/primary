CREATE OR REPLACE TYPE person_t AS OBJECT

-- =============================================
-- Author:      Rishabh Chowdhury
-- Create date: 30-Jan-2022
-- Description: person_t is parent person object  
-- =============================================

(
    firstname                       VARCHAR2(50),
    lastname                        VARCHAR2(50),
    email_id                        VARCHAR2(200),
    phone                           NUMBER,
    
    CONSTRUCTOR FUNCTION person_t RETURN SELF AS RESULT,
    
    CONSTRUCTOR FUNCTION person_t 
    (
        firstname                       VARCHAR2,
        lastname                        VARCHAR2,
        email_id                        VARCHAR2,
        phone                           NUMBER
    )
    RETURN SELF AS RESULT,
    
    NOT FINAL MEMBER FUNCTION obj2string RETURN VARCHAR2
)
NOT FINAL;
/