CREATE OR REPLACE TYPE employee_t under person_t

-- =============================================
-- Author:      Rishabh Chowdhury
-- Create date: 29-Aug-2022
-- Description: employee_t is child of person object  
-- =============================================

(
    emp_id                          NUMBER,
    psno                            NUMBER,
    designation                     VARCHAR2(50 char),
    experience                      NUMBER,
    manager                         VARCHAR2(40 char),
    location                        VARCHAR2(30 char),
    status                          varchar2(10 char),
    cadre                           varchar2(10 char),
    practice_id                     number,
    
    CONSTRUCTOR FUNCTION employee_t RETURN SELF AS RESULT,
    
    CONSTRUCTOR FUNCTION employee_t 
    (
        psno                            NUMBER,
        designation                     VARCHAR2,
        experience                      NUMBER,
        manager                         VARCHAR2,
        location                        VARCHAR2,
        status                          varchar2,
        cadre                           varchar2,
        practice_id                     number
    )
    RETURN SELF AS RESULT,
    
    CONSTRUCTOR FUNCTION employee_t 
    (
        emp_id                          NUMBER,
        psno                            NUMBER,
        designation                     VARCHAR2,
        experience                      NUMBER,
        manager                         VARCHAR2,
        location                        VARCHAR2,
        status                          varchar2,
        cadre                           varchar2,
        practice_id                     number
    )
    RETURN SELF AS RESULT,
    
    CONSTRUCTOR FUNCTION employee_t 
    (
        emp_id                          NUMBER
    )
    RETURN SELF AS RESULT,
    
    OVERRIDING member FUNCTION obj2string RETURN VARCHAR2
)
NOT FINAL;
/