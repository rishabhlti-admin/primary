CREATE OR REPLACE TYPE practice_t AS OBJECT

-- =============================================
-- Author:      Rishabh Chowdhury
-- Create date: 26-Jan-2022
-- Description: practice_t is practice object  
-- =============================================

(
    int_practice_id      NUMBER,
    int_business_unit_id NUMBER,
    practice_name        VARCHAR2(100 CHAR),
    practice_head        VARCHAR2(100 CHAR),
    head_email           VARCHAR2(100 CHAR),
    CONSTRUCTOR FUNCTION practice_t RETURN SELF AS RESULT,
    
    CONSTRUCTOR FUNCTION practice_t 
    (
        int_business_unit_id NUMBER,
        practice_name        VARCHAR2,
        practice_head        VARCHAR2
    )
    RETURN SELF AS RESULT,
    
    CONSTRUCTOR FUNCTION practice_t 
    (
        int_business_unit_id NUMBER,
        practice_name        VARCHAR2,
        practice_head        VARCHAR2,
        head_email           VARCHAR2
    )
    RETURN SELF AS RESULT,
    
    CONSTRUCTOR FUNCTION practice_t 
    (
        int_practice_id      NUMBER,
        int_business_unit_id NUMBER,
        practice_name        VARCHAR2,
        practice_head        VARCHAR2,
        head_email           VARCHAR2
    )
    RETURN SELF AS RESULT,
    
    MEMBER FUNCTION obj2string RETURN VARCHAR2
);
/