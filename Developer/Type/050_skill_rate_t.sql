CREATE OR REPLACE TYPE skill_rate_t AS OBJECT

-- =============================================
-- Author:      Rishabh Chowdhury
-- Create date: 30-Aug-2022
-- Description: skill_rate_t is skill set of person object  
-- =============================================

(
    emp_skill_id                   number,
    skill_set_id                   number,
    experience                     number,
    rating                         number,
    
    CONSTRUCTOR FUNCTION skill_rate_t RETURN SELF AS RESULT,
    
    CONSTRUCTOR FUNCTION skill_rate_t 
    (
        skill_set_id                   number,
        experience                     number,
        rating                         number
    )
    RETURN SELF AS RESULT,
    
    CONSTRUCTOR FUNCTION skill_rate_t 
    (
        emp_skill_id                   number,
        skill_set_id                   number,
        experience                     number,
        rating                         number
    )
    RETURN SELF AS RESULT,
    
    CONSTRUCTOR FUNCTION skill_rate_t 
    (
        emp_skill_id                   number
    )
    RETURN SELF AS RESULT,
    
    MEMBER FUNCTION obj2string RETURN VARCHAR2
);
/