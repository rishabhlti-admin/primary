CREATE OR REPLACE PACKAGE pkg_user_mgmt AUTHID current_user AS

-- =============================================
-- Author:      Rishabh Chowdhury
-- Create date: 18-Apr-2022
-- Description: pkg_user_mgmt is packages containing  
--              code for user management of application
-- =============================================

    /*
    -- Function generates a random password
    */
    FUNCTION generate_password(
        v_length NUMBER DEFAULT 10
    ) RETURN VARCHAR2;
    /*
    -- Procedure creates workspace level user
    */
    PROCEDURE create_user(
                            p_user    IN  user_t
                          );
                          
    

END pkg_user_mgmt;