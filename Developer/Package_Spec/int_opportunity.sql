-- APIs --
create or replace package int_opportunity_master_api
is
 
    /* example:
        declare
            l_project_name                  varchar2(4000);
            l_project_desc                  varchar2(4000);
            l_no_of_positions               integer;
            l_start_date                    date;
            l_end_date                      date;
            l_int_client_master_id          number;
        begin
        int_opportunity_master_api.get_row (
            p_id                            => 1,
            p_project_name                  => l_project_name,
            p_project_desc                  => l_project_desc,
            p_no_of_positions               => l_no_of_positions,
            p_start_date                    => l_start_date,
            p_end_date                      => l_end_date,
            p_int_client_master_id          => l_int_client_master_id
            );
        end;
    */

    procedure get_row (
        p_id                           in number,
        P_project_name                 out varchar2,
        P_project_desc                 out varchar2,
        P_no_of_positions              out integer,
        P_start_date                   out date,
        P_end_date                     out date,
        P_int_client_master_id         out number
    );
 
    /* example:
        begin
        int_opportunity_master_api.insert_row (
            p_id                          => null,
            p_project_name                => null,
            p_project_desc                => null,
            p_no_of_positions             => null,
            p_start_date                  => null,
            p_end_date                    => null,
            p_int_client_master_id        => null
            );
        end;
    */

    procedure insert_row  (
        p_id                           in number default null,
        p_project_name                 in varchar2 default null,
        p_project_desc                 in varchar2 default null,
        p_no_of_positions              in integer default null,
        p_start_date                   in date default null,
        p_end_date                     in date default null,
        p_int_client_master_id         in number default null
    );
    procedure update_row  (
        p_id                           in number default null,
        p_project_name                 in varchar2 default null,
        p_project_desc                 in varchar2 default null,
        p_no_of_positions              in integer default null,
        p_start_date                   in date default null,
        p_end_date                     in date default null,
        p_int_client_master_id         in number default null
    );
    procedure delete_row (
        p_id                           in number
    );
end int_opportunity_master_api;
/

create or replace package int_opportunity_breakup_api
is
 
    /* example:
        declare
            l_int_opportunity_master_id     number;
            l_resource_seq                  integer;
            l_experience                    varchar2(20);
            l_cadre                         varchar2(20);
            l_spoc                          varchar2(4000);
            l_location                      varchar2(4000);
            l_int_employee_id               number;
            l_int_practice_id               number;
        begin
        int_opportunity_breakup_api.get_row (
            p_id                            => 1,
            p_int_opportunity_master_id     => l_int_opportunity_master_id,
            p_resource_seq                  => l_resource_seq,
            p_experience                    => l_experience,
            p_cadre                         => l_cadre,
            p_spoc                          => l_spoc,
            p_location                      => l_location,
            p_int_employee_id               => l_int_employee_id,
            p_int_practice_id               => l_int_practice_id
            );
        end;
    */

    procedure get_row (
        p_id                           in number,
        P_int_opportunity_master_id    out number,
        P_resource_seq                 out integer,
        P_experience                   out varchar2,
        P_cadre                        out varchar2,
        P_spoc                         out varchar2,
        P_location                     out varchar2,
        P_int_employee_id              out number,
        P_int_practice_id              out number
    );
 
    /* example:
        begin
        int_opportunity_breakup_api.insert_row (
            p_id                          => null,
            p_int_opportunity_master_id   => null,
            p_resource_seq                => null,
            p_experience                  => null,
            p_cadre                       => null,
            p_spoc                        => null,
            p_location                    => null,
            p_int_employee_id             => null,
            p_int_practice_id             => null
            );
        end;
    */

    procedure insert_row  (
        p_id                           in number default null,
        p_int_opportunity_master_id    in number default null,
        p_resource_seq                 in integer default null,
        p_experience                   in varchar2 default null,
        p_cadre                        in varchar2 default null,
        p_spoc                         in varchar2 default null,
        p_location                     in varchar2 default null,
        p_int_employee_id              in number default null,
        p_int_practice_id              in number default null
    );
    procedure update_row  (
        p_id                           in number default null,
        p_int_opportunity_master_id    in number default null,
        p_resource_seq                 in integer default null,
        p_experience                   in varchar2 default null,
        p_cadre                        in varchar2 default null,
        p_spoc                         in varchar2 default null,
        p_location                     in varchar2 default null,
        p_int_employee_id              in number default null,
        p_int_practice_id              in number default null
    );
    procedure delete_row (
        p_id                           in number
    );
end int_opportunity_breakup_api;
/
