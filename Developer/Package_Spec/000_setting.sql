CREATE OR REPLACE PACKAGE settings AUTHID current_user AS

-- =============================================
-- Author:      Rishabh Chowdhury
-- Create date: 18-Apr-2022
-- Description: settings is packages containing  
--              code for general setting of application
-- =============================================

    agv_default_schema CONSTANT VARCHAR2(40) := 'LTI_INTERVIEW';
    agv_workspace_name CONSTANT VARCHAR2(40) := 'LTI_INTERVIEW';
    agv_app_name       CONSTANT VARCHAR2(40) := 'i-interview';
    agv_email_from     CONSTANT VARCHAR2(100) := 'nadeem1.khan@lntinfotech.com';
    
    subtype dt_fullname is varchar2(100);
    
    --TYPE t_num is table of number;
    
    FUNCTION generate_series (
                                  p_start       IN number,
                                  p_total_val   IN number,
                                  p_offset      IN number DEFAULT 1
                              )
    RETURN t_num PIPELINED DETERMINISTIC;

END settings;