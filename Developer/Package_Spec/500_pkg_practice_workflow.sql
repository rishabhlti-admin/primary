CREATE OR REPLACE PACKAGE pkg_practice_workflow AUTHID current_user AS

-- =============================================
-- Author:      Rishabh Chowdhury
-- Create date: 28-Jan-2022
-- Description: pkg_practice_workflow is packages containing  
--              code for practice workflows.
-- =============================================
    
    gv_practice_create_err_id  CONSTANT NUMBER        := -20001;
    gv_practice_create_err_msg CONSTANT VARCHAR2(200) := 'Practice creation failed';
    gv_practice_edit_err_id    CONSTANT NUMBER        := -20002;
    gv_practice_edit_err_msg   CONSTANT VARCHAR2(200) := 'Practice updation failed';
    gv_practice_del_err_id     CONSTANT NUMBER        := -20003;
    gv_practice_del_err_msg    CONSTANT VARCHAR2(200) := 'Practice deletion failed';
    
    /*
    -- practice name getter based on practice id
    */
    FUNCTION get_practice (p_practice_id IN number) 
    return VARCHAR2 result_cache;
    
    /*
    -- procedure to be called from apex for creating a new practise 
    */
    PROCEDURE create_practice (
        p_practice IN practice_t
    );
    
    /*
    -- procedure to be called from apex for updating an existing practise 
    */
    PROCEDURE edit_practice (
        p_practice IN practice_t
    );
    
    /*
    -- procedure to be called from apex for deletion of an existing practise 
    */
    PROCEDURE delete_practice (
        p_practice IN practice_t
    );

END pkg_practice_workflow;