CREATE OR REPLACE PACKAGE pkg_bu_workflow AUTHID current_user AS

-- =============================================
-- Author:      Rishabh Chowdhury
-- Create date: 29-Jan-2022
-- Description: pkg_bu_workflow is packages containing  
--              code for business unit workflows.
-- =============================================
    
    gv_bu_create_err_id  CONSTANT NUMBER        := -20004;
    gv_bu_create_err_msg CONSTANT VARCHAR2(200) := 'Business Unit creation failed';
    gv_bu_edit_err_id    CONSTANT NUMBER        := -20005;
    gv_bu_edit_err_msg   CONSTANT VARCHAR2(200) := 'Business Unit updation failed';
    gv_bu_del_err_id     CONSTANT NUMBER        := -20006;
    gv_bu_del_err_msg    CONSTANT VARCHAR2(200) := 'Business Unit deletion failed';
    
    /*
    -- BU name getter based on BU id
    */
    FUNCTION get_bu (p_bu_id IN number) 
    return VARCHAR2 result_cache;
    
    /*
    -- procedure to be called from apex for creating a new BU 
    */
    PROCEDURE create_bu (
        p_bu IN business_unit_t
    );
    
    /*
    -- procedure to be called from apex for updating an existing BU 
    */
    PROCEDURE edit_bu (
        p_bu IN business_unit_t
    );
    
    /*
    -- procedure to be called from apex for deletion of an existing BU 
    */
    PROCEDURE delete_bu (
        p_bu IN business_unit_t
    );

END pkg_bu_workflow;