-- APIs --
create or replace package int_client_master_api
is
 
    /* example:
        declare
            l_name                          varchar2(4000);
            l_interview                     varchar2(4000);
            l_onsite_offsite                varchar2(4000);
        begin
        int_client_master_api.get_row (
            p_id                            => 1,
            p_name                          => l_name,
            p_interview                     => l_interview,
            p_onsite_offsite                => l_onsite_offsite
            );
        end;
    */

    procedure get_row (
        p_id                           in number,
        P_name                         out varchar2,
        P_interview                    out varchar2,
        P_onsite_offsite               out varchar2
    );
 
    /* example:
        begin
        int_client_master_api.insert_row (
            p_id                          => null,
            p_name                        => null,
            p_interview                   => null,
            p_onsite_offsite              => null
            );
        end;
    */

    procedure insert_row  (
        p_id                           in number default null,
        p_name                         in varchar2 default null,
        p_interview                    in varchar2 default null,
        p_onsite_offsite               in varchar2 default null
    );
    procedure update_row  (
        p_id                           in number default null,
        p_name                         in varchar2 default null,
        p_interview                    in varchar2 default null,
        p_onsite_offsite               in varchar2 default null
    );
    procedure delete_row (
        p_id                           in number
    );
end int_client_master_api;
/
