CREATE OR REPLACE PACKAGE pkg_employee_workflow AUTHID current_user AS

-- =============================================
-- Author:      Rishabh Chowdhury
-- Create date: 29-Aug-2022
-- Description: pkg_employee_workflow is packages containing  
--              code for employee workflows.
-- =============================================
    
    gv_employee_create_err_id  CONSTANT NUMBER        := -20007;
    gv_employee_create_err_msg CONSTANT VARCHAR2(200) := 'Employee creation failed';
    gv_employee_edit_err_id    CONSTANT NUMBER        := -20008;
    gv_employee_edit_err_msg   CONSTANT VARCHAR2(200) := 'Employee updation failed';
    gv_employee_del_err_id     CONSTANT NUMBER        := -20009;
    gv_employee_del_err_msg    CONSTANT VARCHAR2(200) := 'Employee deletion failed';
    gv_employee_skl_err_id     CONSTANT NUMBER        := -20010;
    gv_employee_skl_err_msg    CONSTANT VARCHAR2(200) := 'Employee skill addition failed';
    gv_emp_skl_edit_err_id     CONSTANT NUMBER        := -20011;
    gv_emp_skl_edit_err_msg    CONSTANT VARCHAR2(200) := 'Employee skill updation failed';
    gv_emp_skl_del_err_id      CONSTANT NUMBER        := -20012;
    gv_emp_skl_del_err_msg     CONSTANT VARCHAR2(200) := 'Employee skill deletion failed';
    
    /*
    -- employee name getter based on employee id
    */
    FUNCTION get_employee (p_employee_id IN number) 
    return VARCHAR2 result_cache;
    
    /*
    -- procedure to be called from apex for creating a new employee 
    */
    PROCEDURE create_employee (
        p_employee IN employee_t
    );
    
    /*
    -- procedure to be called from apex for updating an existing employee 
    */
    PROCEDURE edit_employee (
        p_employee IN employee_t
    );
    
    /*
    -- procedure to be called from apex for deletion of an existing employee 
    */
    PROCEDURE delete_employee (
        p_employee IN employee_t
    );
    
    /*
    -- Procedure to be used for creating employee skill set
    */
    PROCEDURE add_skill_set (
        p_employee IN employee_t,
        p_skills   IN skill_rate_t
    );
    
    /*
    -- Procedure to be used for updating employee skill set
    */
    PROCEDURE update_skill_set (
        p_skills   IN skill_rate_t
    );
    
    /*
    -- Procedure to be used for deleting employee skill set
    */
    PROCEDURE delete_skill_set (
        p_skills   IN skill_rate_t
    );
    
    /*
    -- Procedure to be used for creating slots for employee
    */
    PROCEDURE add_slots (
        p_employee  IN employee_t,
        p_int_dt    IN DATE DEFAULT SYSDATE,
        p_st_time   IN VARCHAR2 DEFAULT to_char(CURRENT_TIMESTAMP, 'HH24:MI:SS'),
        p_duration  IN NUMBER,
        p_status    IN VARCHAR2 DEFAULT 'Provided',
        p_next_days IN NUMBER DEFAULT 1,
        p_weekends  IN VARCHAR2 
    );

END pkg_employee_workflow;