create or replace package  body int_opportunity_master_api
is
 
    procedure get_row (
        p_id                           in number,
        P_project_name                 out varchar2,
        P_project_desc                 out varchar2,
        P_no_of_positions              out integer,
        P_start_date                   out date,
        P_end_date                     out date,
        P_int_client_master_id         out number
    )
    is
    begin
        for c1 in (select * from int_opportunity_master where id = p_id) loop
            p_project_name := c1.project_name;
            p_project_desc := c1.project_desc;
            p_no_of_positions := c1.no_of_positions;
            p_start_date := c1.start_date;
            p_end_date := c1.end_date;
            p_int_client_master_id := c1.int_client_master_id;
        end loop;
    end get_row;

 
    procedure insert_row  (
        p_id                           in number default null,
        p_project_name                 in varchar2 default null,
        p_project_desc                 in varchar2 default null,
        p_no_of_positions              in integer default null,
        p_start_date                   in date default null,
        p_end_date                     in date default null,
        p_int_client_master_id         in number default null
    )
    is
    begin
        insert into int_opportunity_master (
            id,
            project_name,
            project_desc,
            no_of_positions,
            start_date,
            end_date,
            int_client_master_id
        ) values (
            p_id,
            p_project_name,
            p_project_desc,
            p_no_of_positions,
            p_start_date,
            p_end_date,
            p_int_client_master_id
        );
    end insert_row;

    procedure update_row  (
        p_id                           in number default null,
        p_project_name                 in varchar2 default null,
        p_project_desc                 in varchar2 default null,
        p_no_of_positions              in integer default null,
        p_start_date                   in date default null,
        p_end_date                     in date default null,
        p_int_client_master_id         in number default null
    )
    is
    begin
        update  int_opportunity_master set 
            id = p_id,
            project_name = p_project_name,
            project_desc = p_project_desc,
            no_of_positions = p_no_of_positions,
            start_date = p_start_date,
            end_date = p_end_date,
            int_client_master_id = p_int_client_master_id
        where id = p_id;
    end update_row;

    procedure delete_row (
        p_id                           in number
    )
    is
    begin
        delete from int_opportunity_master where id = p_id;
    end delete_row;

end int_opportunity_master_api;
/

create or replace package  body int_opportunity_breakup_api
is
 
    procedure get_row (
        p_id                           in number,
        P_int_opportunity_master_id    out number,
        P_resource_seq                 out integer,
        P_experience                   out varchar2,
        P_cadre                        out varchar2,
        P_spoc                         out varchar2,
        P_location                     out varchar2,
        P_int_employee_id              out number,
        P_int_practice_id              out number
    )
    is
    begin
        for c1 in (select * from int_opportunity_breakup where id = p_id) loop
            p_int_opportunity_master_id := c1.int_opportunity_master_id;
            p_resource_seq := c1.resource_seq;
            p_experience := c1.experience;
            p_cadre := c1.cadre;
            p_spoc := c1.spoc;
            p_location := c1.location;
            p_int_employee_id := c1.int_employee_id;
            p_int_practice_id := c1.int_practice_id;
        end loop;
    end get_row;

 
    procedure insert_row  (
        p_id                           in number default null,
        p_int_opportunity_master_id    in number default null,
        p_resource_seq                 in integer default null,
        p_experience                   in varchar2 default null,
        p_cadre                        in varchar2 default null,
        p_spoc                         in varchar2 default null,
        p_location                     in varchar2 default null,
        p_int_employee_id              in number default null,
        p_int_practice_id              in number default null
    )
    is
    begin
        insert into int_opportunity_breakup (
            id,
            int_opportunity_master_id,
            resource_seq,
            experience,
            cadre,
            spoc,
            location,
            int_employee_id,
            int_practice_id
        ) values (
            p_id,
            p_int_opportunity_master_id,
            p_resource_seq,
            p_experience,
            p_cadre,
            p_spoc,
            p_location,
            p_int_employee_id,
            p_int_practice_id
        );
    end insert_row;

    procedure update_row  (
        p_id                           in number default null,
        p_int_opportunity_master_id    in number default null,
        p_resource_seq                 in integer default null,
        p_experience                   in varchar2 default null,
        p_cadre                        in varchar2 default null,
        p_spoc                         in varchar2 default null,
        p_location                     in varchar2 default null,
        p_int_employee_id              in number default null,
        p_int_practice_id              in number default null
    )
    is
    begin
        update  int_opportunity_breakup set 
            id = p_id,
            int_opportunity_master_id = p_int_opportunity_master_id,
            resource_seq = p_resource_seq,
            experience = p_experience,
            cadre = p_cadre,
            spoc = p_spoc,
            location = p_location,
            int_employee_id = p_int_employee_id,
            int_practice_id = p_int_practice_id
        where id = p_id;
    end update_row;

    procedure delete_row (
        p_id                           in number
    )
    is
    begin
        delete from int_opportunity_breakup where id = p_id;
    end delete_row;

end int_opportunity_breakup_api;
/


