CREATE OR REPLACE PACKAGE BODY pkg_bu_workflow 
AS

-- =============================================
-- Author:      Rishabh Chowdhury
-- Create date: 29-Jan-2022
-- Description: pkg_bu_workflow is packages containing  
--              code for business unit workflows.
-- =============================================
    
    gc_scope_prefix CONSTANT VARCHAR2(31) := lower($$PLSQL_UNIT) || '.';
    
    /*
    -- BU name getter based on BU id
    */
    FUNCTION get_bu (p_bu_id IN number) 
    return VARCHAR2 result_cache
    IS
    $IF DBMS_DB_VERSION.VER_LE_11_2
    $THEN
       /* UDF pragma not available till 12.1 */
    $ELSE
       PRAGMA UDF;
    $END 
    l_scope logger_logs.scope%type := gc_scope_prefix || 'get_bu';
    l_params logger.tab_param;
    v_bu int_business_unit.bu_name%type;
    BEGIN 
        logger.append_param(l_params, 'p_bu_id - ', p_bu_id);
        SELECT
            bu_name
        INTO
            v_bu
        FROM
            int_business_unit
        WHERE
            int_business_unit_id = p_bu_id;
        RETURN v_bu;
    EXCEPTION
        WHEN OTHERS THEN
            logger.log_error('Unhandled Exception', l_scope, dbms_utility.Format_error_backtrace(), l_params);
            RAISE;
    END get_bu;
    
    /*
    -- procedure to be called from apex for creating a new BU 
    */
    PROCEDURE create_bu (
        p_bu IN business_unit_t
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'create_bu';
    l_params logger.tab_param;
    BEGIN
        logger.append_param(l_params, 'p_bu - ', p_bu.obj2string);
        logger.log('START', l_scope, null, l_params);
            int_business_unit_api.insert_row
            (
                P_bu_name                => p_bu.bu_name,
                P_bu_head                => p_bu.bu_head
            );
        logger.log('END', l_scope);
    EXCEPTION
      WHEN OTHERS THEN
      logger.log_error('Unhandled Exception', l_scope, dbms_utility.Format_error_backtrace(), l_params);
      RAISE_APPLICATION_ERROR(gv_bu_create_err_id, gv_bu_create_err_msg);    
    END create_bu;
    
    /*
    -- procedure to be called from apex for updating an existing BU 
    */
    PROCEDURE edit_bu (
        p_bu IN business_unit_t
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'edit_bu';
    l_params logger.tab_param;
    BEGIN
        logger.append_param(l_params, 'p_bu - ', p_bu.obj2string);
        logger.log('START', l_scope, null, l_params);
            int_business_unit_api.update_row
            (
                p_id                     => p_bu.int_business_unit_id,  
                P_bu_name                => p_bu.bu_name,
                P_bu_head                => p_bu.bu_head
            );
        logger.log('END', l_scope);
    EXCEPTION
      WHEN OTHERS THEN
      logger.log_error('Unhandled Exception', l_scope, dbms_utility.Format_error_backtrace(), l_params);
      RAISE_APPLICATION_ERROR(gv_bu_edit_err_id, gv_bu_edit_err_msg);    
    END edit_bu;
    
    /*
    -- procedure to be called from apex for deletion of an existing BU 
    */
    PROCEDURE delete_bu (
        p_bu IN business_unit_t
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'delete_bu';
    l_params logger.tab_param;
    BEGIN
        logger.append_param(l_params, 'p_bu - ', p_bu.obj2string);
        logger.log('START', l_scope, null, l_params);
            int_business_unit_api.delete_row
            (
                p_id                           => p_bu.int_business_unit_id
            );
        logger.log('END', l_scope);
    EXCEPTION
      WHEN OTHERS THEN
      logger.log_error('Unhandled Exception', l_scope, dbms_utility.Format_error_backtrace(), l_params);
      RAISE_APPLICATION_ERROR(gv_bu_del_err_id, gv_bu_del_err_msg);    
    END delete_bu;
    
begin
    DBMS_APPLICATION_INFO.set_client_info(client_info => sys_guid() || ':SQL-Developer');
END pkg_bu_workflow;