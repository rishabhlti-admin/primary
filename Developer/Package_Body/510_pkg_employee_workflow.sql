CREATE OR REPLACE PACKAGE BODY pkg_employee_workflow 
AS

-- =============================================
-- Author:      Rishabh Chowdhury
-- Create date: 29-Aug-2022
-- Description: pkg_employee_workflow is packages containing  
--              code for employee workflows.
-- =============================================

    gc_scope_prefix CONSTANT VARCHAR2(31) := lower($$PLSQL_UNIT) || '.';
    
    /*
    -- employee name getter based on employee id
    */
    FUNCTION get_employee (p_employee_id IN number) 
    return VARCHAR2 result_cache
    IS
    $IF DBMS_DB_VERSION.VER_LE_11_2
    $THEN
       /* UDF pragma not available till 12.1 */
    $ELSE
       PRAGMA UDF;
    $END 
    l_scope logger_logs.scope%type := gc_scope_prefix || 'get_employee';
    l_params logger.tab_param;
    v_employee settings.dt_fullname;
    BEGIN 
        logger.append_param(l_params, 'p_employee_id - ', p_employee_id);
        SELECT
            first_name || ' ' || last_name
        INTO
            v_employee
        FROM
            int_employee
        WHERE
            int_employee_id = p_employee_id;
        RETURN v_employee;
    EXCEPTION
        WHEN OTHERS THEN
            logger.log_error('Unhandled Exception', l_scope, null, l_params);
            RAISE;
    END get_employee; 
    
    /*
    -- procedure to be called from apex for creating a new employee 
    */
    PROCEDURE create_employee (
        p_employee IN employee_t
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'create_employee';
    l_params logger.tab_param;
    BEGIN
        logger.append_param(l_params, 'p_employee - ', p_employee.obj2string);
        logger.log('START', l_scope, null, l_params);
            int_employee_api.insert_row (
                p_psno                        => p_employee.psno,
                p_first_name                  => p_employee.firstname,
                p_last_name                   => p_employee.lastname,
                p_mobno                       => p_employee.phone,
                p_email_id                    => p_employee.email_id,
                p_designation                 => p_employee.designation,
                p_total_experience            => p_employee.experience,
                p_status                      => p_employee.status,
                p_reporting_manager           => p_employee.manager,
                p_location                    => p_employee.location,
                p_cadre                       => p_employee.cadre,
                p_practice_id                 => p_employee.practice_id
            );
        logger.log('END', l_scope);
    EXCEPTION
      WHEN OTHERS THEN
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      RAISE_APPLICATION_ERROR(gv_employee_create_err_id, gv_employee_create_err_msg);    
    END create_employee;
    
    /*
    -- procedure to be called from apex for updating an existing practise 
    */
    PROCEDURE edit_employee (
        p_employee IN employee_t
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'edit_employee';
    l_params logger.tab_param;
    BEGIN
        logger.append_param(l_params, 'p_employee - ', p_employee.obj2string);
        logger.log('START', l_scope, null, l_params);
            int_employee_api.update_row
            (
                p_int_employee_id             => p_employee.emp_id,
                p_psno                        => p_employee.psno,
                p_first_name                  => p_employee.firstname,
                p_last_name                   => p_employee.lastname,
                p_mobno                       => p_employee.phone,
                p_email_id                    => p_employee.email_id,
                p_designation                 => p_employee.designation,
                p_total_experience            => p_employee.experience,
                p_status                      => p_employee.status,
                p_reporting_manager           => p_employee.manager,
                p_location                    => p_employee.location,
                p_cadre                       => p_employee.cadre,
                p_practice_id                 => p_employee.practice_id
            );
        logger.log('END', l_scope);
    EXCEPTION
      WHEN OTHERS THEN
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      RAISE_APPLICATION_ERROR(gv_employee_edit_err_id, gv_employee_edit_err_msg);    
    END edit_employee;
    
    /*
    -- procedure to be called from apex for deletion of an existing practise 
    */
    PROCEDURE delete_employee (
        p_employee IN employee_t
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'delete_employee';
    l_params logger.tab_param;
    BEGIN
        logger.append_param(l_params, 'p_employee - ', p_employee.obj2string);
        logger.log('START', l_scope, null, l_params);
            int_employee_api.delete_row
            (
                p_int_employee_id                           => p_employee.emp_id
            );
        logger.log('END', l_scope);
    EXCEPTION
      WHEN OTHERS THEN
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      RAISE_APPLICATION_ERROR(gv_employee_del_err_id, gv_employee_del_err_msg);    
    END delete_employee;
    
    /*
    -- Procedure to be used for creating employee skill set
    */
    PROCEDURE add_skill_set (
        p_employee IN employee_t,
        p_skills   IN skill_rate_t
    )
    IS
    l_scope logger_logs.scope%type := gc_scope_prefix || 'add_skill_set';
    l_params logger.tab_param;
    BEGIN
        logger.append_param(l_params, 'p_employee - ', p_employee.obj2string);
        logger.append_param(l_params, 'p_skills - ', p_skills.obj2string);
        logger.log('START', l_scope, null, l_params);
        int_employee_skill_master_api.insert_row (
            p_employee_id        => p_employee.emp_id,
            p_rating             => p_skills.rating,
            p_experience         => p_skills.experience,
            p_skill_set_id       => p_skills.skill_set_id
        );
        logger.log('END', l_scope);
    EXCEPTION
      WHEN OTHERS THEN
          ROLLBACK;
          logger.log_error('Unhandled Exception', l_scope, null, l_params);
          RAISE_APPLICATION_ERROR(gv_employee_skl_err_id, gv_employee_skl_err_msg);    
    END add_skill_set;
    
    /*
    -- Procedure to be used for updating employee skill set
    */
    PROCEDURE update_skill_set (
        p_skills   IN skill_rate_t
    )
    IS
    l_scope logger_logs.scope%type := gc_scope_prefix || 'update_skill_set';
    l_params logger.tab_param;
    BEGIN
        logger.append_param(l_params, 'p_skills - ', p_skills.obj2string);
        logger.log('START', l_scope, null, l_params);
        int_employee_skill_master_api.update_row  (
            p_int_employee_skill_master_id => p_skills.emp_skill_id,
            p_rating                       => p_skills.rating,
            p_experience                   => p_skills.experience,
            p_skill_set_id                 => p_skills.skill_set_id
        );
        logger.log('END', l_scope);
    EXCEPTION
      WHEN OTHERS THEN
          ROLLBACK;
          logger.log_error('Unhandled Exception', l_scope, null, l_params);
          RAISE_APPLICATION_ERROR(gv_emp_skl_edit_err_id, gv_emp_skl_edit_err_msg); 
    END update_skill_set;
    
    /*
    -- Procedure to be used for deleting employee skill set
    */
    PROCEDURE delete_skill_set (
        p_skills   IN skill_rate_t
    )
    IS
    l_scope logger_logs.scope%type := gc_scope_prefix || 'delete_skill_set';
    l_params logger.tab_param;
    BEGIN
        logger.append_param(l_params, 'p_skills - ', p_skills.obj2string);
        logger.log('START', l_scope, null, l_params);
        int_employee_skill_master_api.delete_row  (
            p_int_employee_skill_master_id => p_skills.emp_skill_id
        );
        logger.log('END', l_scope);
    EXCEPTION
      WHEN OTHERS THEN
          ROLLBACK;
          logger.log_error('Unhandled Exception', l_scope, null, l_params);
          RAISE_APPLICATION_ERROR(gv_emp_skl_del_err_id, gv_emp_skl_del_err_msg);
    END delete_skill_set;
    
    /*
    -- Procedure to be used for creating slots for employee
    */
    PROCEDURE add_slots (
        p_employee  IN employee_t,
        p_int_dt    IN DATE DEFAULT SYSDATE,
        p_st_time   IN VARCHAR2 DEFAULT to_char(CURRENT_TIMESTAMP, 'HH24:MI:SS'),
        p_duration  IN NUMBER,
        p_status    IN VARCHAR2 DEFAULT 'Provided',
        p_next_days IN NUMBER DEFAULT 1,
        p_weekends  IN VARCHAR2
    )
    IS
    lv_st_time TIMESTAMP;
    BEGIN
        FOR i in 1 .. p_next_days
        LOOP
            lv_st_time := TO_TIMESTAMP( to_char(p_int_dt + i - 1, 'DD-MON-YYYY') || ' ' || p_st_time, 'DD-MON-YYYY HH24:MI:SS');
            
            int_employee_int_slot_api.insert_row  (
                p_employee_id                  => p_employee.emp_id,
                p_interview_dt                 => p_int_dt + i - 1, -- case
                p_start_time                   => lv_st_time,
                p_end_time                     => lv_st_time + NUMTODSINTERVAL(p_duration, 'minute'),
                p_status                       => p_status
            );
        END LOOP;
    END add_slots;
    
begin
    DBMS_APPLICATION_INFO.set_client_info(client_info => sys_guid() || ':SQL-Developer');    
END pkg_employee_workflow;