CREATE OR REPLACE PACKAGE BODY settings  AS

-- =============================================
-- Author:      Rishabh Chowdhury
-- Create date: 31-Aug-2022
-- Description: settings is packages containing  
--              code for general setting of application
-- =============================================
    
    FUNCTION generate_series (
                                  p_start       IN number,
                                  p_total_val   IN number,
                                  p_offset      IN number DEFAULT 1
                              )
    RETURN t_num PIPELINED DETERMINISTIC
    IS
    lv_start   NUMBER;
    lv_counter NUMBER := 1;
    v_out      t_num := t_num();
    BEGIN
        lv_start := p_start;
        WHILE lv_counter <= p_total_val
        LOOP
            v_out.EXTEND;
            v_out(lv_counter) := lv_start;
            pipe row (lv_start);
            lv_start := lv_start + p_offset;
            lv_counter := lv_counter + 1;
        END LOOP;
    END generate_series;

END settings;