CREATE OR REPLACE PACKAGE BODY pkg_practice_workflow 
AS

-- =============================================
-- Author:      Rishabh Chowdhury
-- Create date: 28-Jan-2022
-- Description: pkg_practice_workflow is packages containing  
--              code for practice workflows.
-- =============================================

    gc_scope_prefix CONSTANT VARCHAR2(31) := lower($$PLSQL_UNIT) || '.';
    
    /*
    -- practice name getter based on practice id
    */
    FUNCTION get_practice (p_practice_id IN number) 
    return VARCHAR2 result_cache
    IS
    $IF DBMS_DB_VERSION.VER_LE_11_2
    $THEN
       /* UDF pragma not available till 12.1 */
    $ELSE
       PRAGMA UDF;
    $END 
    l_scope logger_logs.scope%type := gc_scope_prefix || 'get_practice';
    l_params logger.tab_param;
    v_practice int_practice.practice_name%type;
    BEGIN 
        logger.append_param(l_params, 'p_practice_id - ', p_practice_id);
        SELECT
            practice_name
        INTO
            v_practice
        FROM
            int_practice
        WHERE
            int_practice_id = p_practice_id;
        RETURN v_practice;
    EXCEPTION
        WHEN OTHERS THEN
            logger.log_error('Unhandled Exception', l_scope, null, l_params);
            RAISE;
    END get_practice; 
    
    /*
    -- procedure to be called from apex for creating a new practise 
    */
    PROCEDURE create_practice (
        p_practice IN practice_t
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'create_practice';
    l_params logger.tab_param;
    BEGIN
        logger.append_param(l_params, 'p_practice - ', p_practice.obj2string);
        logger.log('START', l_scope, null, l_params);
            int_practice_api.insert_row
            (
                p_int_business_unit_id         => p_practice.int_business_unit_id,
                p_practice_name                => p_practice.practice_name,
                p_practice_head                => p_practice.practice_head,
                p_head_email                   => p_practice.head_email
            );
        logger.log('END', l_scope);
    EXCEPTION
      WHEN OTHERS THEN
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      RAISE_APPLICATION_ERROR(gv_practice_create_err_id, gv_practice_create_err_msg);    
    END create_practice;
    
    /*
    -- procedure to be called from apex for updating an existing practise 
    */
    PROCEDURE edit_practice (
        p_practice IN practice_t
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'edit_practice';
    l_params logger.tab_param;
    BEGIN
        logger.append_param(l_params, 'p_practice - ', p_practice.obj2string);
        logger.log('START', l_scope, null, l_params);
            int_practice_api.update_row
            (
                p_id                           => p_practice.int_practice_id,
                p_int_business_unit_id         => p_practice.int_business_unit_id,
                p_practice_name                => p_practice.practice_name,
                p_practice_head                => p_practice.practice_head,
                p_head_email                   => p_practice.head_email
            );
        logger.log('END', l_scope);
    EXCEPTION
      WHEN OTHERS THEN
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      RAISE_APPLICATION_ERROR(gv_practice_edit_err_id, gv_practice_edit_err_msg);    
    END edit_practice;
    
    /*
    -- procedure to be called from apex for deletion of an existing practise 
    */
    PROCEDURE delete_practice (
        p_practice IN practice_t
    )
    is
    l_scope logger_logs.scope%type := gc_scope_prefix || 'delete_practice';
    l_params logger.tab_param;
    BEGIN
        logger.append_param(l_params, 'p_practice - ', p_practice.obj2string);
        logger.log('START', l_scope, null, l_params);
            int_practice_api.delete_row
            (
                p_id                           => p_practice.int_practice_id
            );
        logger.log('END', l_scope);
    EXCEPTION
      WHEN OTHERS THEN
      logger.log_error('Unhandled Exception', l_scope, null, l_params);
      RAISE_APPLICATION_ERROR(gv_practice_del_err_id, gv_practice_del_err_msg);    
    END delete_practice;
    
begin
    DBMS_APPLICATION_INFO.set_client_info(client_info => sys_guid() || ':SQL-Developer');    
END pkg_practice_workflow;