create or replace package  body int_client_master_api
is
 
    procedure get_row (
        p_id                           in number,
        P_name                         out varchar2,
        P_interview                    out varchar2,
        P_onsite_offsite               out varchar2
    )
    is
    begin
        for c1 in (select * from int_client_master where id = p_id) loop
            p_name := c1.name;
            p_interview := c1.interview;
            p_onsite_offsite := c1.onsite_offsite;
        end loop;
    end get_row;

 
    procedure insert_row  (
        p_id                           in number default null,
        p_name                         in varchar2 default null,
        p_interview                    in varchar2 default null,
        p_onsite_offsite               in varchar2 default null
    )
    is
    begin
        insert into int_client_master (
            id,
            name,
            interview,
            onsite_offsite
        ) values (
            p_id,
            p_name,
            p_interview,
            p_onsite_offsite
        );
    end insert_row;

    procedure update_row  (
        p_id                           in number default null,
        p_name                         in varchar2 default null,
        p_interview                    in varchar2 default null,
        p_onsite_offsite               in varchar2 default null
    )
    is
    begin
        update  int_client_master set 
            id = p_id,
            name = p_name,
            interview = p_interview,
            onsite_offsite = p_onsite_offsite
        where id = p_id;
    end update_row;

    procedure delete_row (
        p_id                           in number
    )
    is
    begin
        delete from int_client_master where id = p_id;
    end delete_row;

end int_client_master_api;
/
