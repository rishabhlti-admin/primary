CREATE OR REPLACE PACKAGE BODY pkg_user_mgmt 
AS

-- =============================================
-- Author:      Rishabh Chowdhury
-- Create date: 18-Apr-2022
-- Description: pkg_user_mgmt is packages containing  
--              code for user management of application
-- =============================================
    
    gc_scope_prefix CONSTANT VARCHAR2(31) := lower($$PLSQL_UNIT) || '.';
    
    /*
    -- Function generates a random password
    */
    FUNCTION generate_password(
        v_length NUMBER DEFAULT 10
    ) RETURN VARCHAR2 IS
        my_str VARCHAR2(4000);
    BEGIN
        FOR i IN 1..v_length LOOP my_str := my_str
                                            || dbms_random.string(
            CASE
                WHEN dbms_random.value(0, 1) < 0.5 THEN
                    'l'
                ELSE 'x'
            END, 1);
        END LOOP;
    
        RETURN my_str;
    END generate_password;

    /*
    -- Procedure creates workspace level user
    */
    PROCEDURE create_user(
                             p_user    IN  user_t
                          )
    IS
    l_scope logger_logs.scope%type := gc_scope_prefix || 'create_user';
    l_params logger.tab_param;
    
    v_password      varchar2(50);
    v_workspace_id  number;
    v_group_id      number;
    BEGIN
        logger.append_param(l_params, 'p_person - ', p_user.obj2string);
        logger.log('START', l_scope, null, l_params);
        
        v_password := generate_password;
        logger.log_info('Password Generated', l_scope, 'Password Generated= ' || v_password, l_params);
        
        --Getting workspace id
        v_workspace_id := apex_util.find_security_group_id (p_workspace => settings.agv_workspace_name);
        logger.log_info('Workspace id - ', l_scope, 'Workspace id = ' || v_workspace_id, l_params);

        -- Set security group
        apex_util.set_security_group_id (p_security_group_id => v_workspace_id);

        v_group_id := apex_util.get_group_id(null);
        APEX_UTIL.CREATE_USER(
            p_user_name                     => p_user.username,
            p_first_name                    => p_user.firstname,
            p_last_name                     => p_user.lastname,
            p_description                   => 'Developer added for development on' || to_char(sysdate , 'DD-MON-YYYY'),
            p_email_address                 => p_user.email_id,
            p_web_password                  => v_password,
            p_group_ids                     => v_group_id,
            p_developer_privs               => 'ADMIN:CREATE:DATA_LOADER:EDIT:HELP:MONITOR:SQL',
            p_default_schema                => settings.agv_default_schema,
            p_allow_access_to_schemas       => settings.agv_default_schema,
            p_change_password_on_first_use  => 'Y',
            p_attribute_01                  => p_user.phone);
            
        apex_mail.send (
            p_from               => settings.agv_email_from, 
            p_to                 => p_user.email_id,
            p_template_static_id => 'USERCREATION',
            p_placeholders       => '{' ||
            '    "application_name":'    || apex_json.stringify( settings.agv_app_name ) ||
            '   ,"username":'            || apex_json.stringify( p_user.username ) ||
            '   ,"firstname":'           || apex_json.stringify( p_user.firstname ) ||
            '   ,"lastname":'            || apex_json.stringify( p_user.lastname ) ||
            '   ,"password":'            || apex_json.stringify( v_password ) ||
            '   ,"START_TIME":'          || apex_json.stringify( ' ' ) ||
            '   ,"DURATION":'            || apex_json.stringify( ' ' ) ||
            '   ,"LOCATION":'            || apex_json.stringify( ' ' ) ||
            '   ,"URL_LINK":'            || apex_json.stringify( ' ' ) ||
            '   ,"URL_NAME":'            || apex_json.stringify( ' ' ) ||
            '   ,"NOTES":'               || apex_json.stringify( ' ' ) ||
            '   ,"MY_APPLICATION_LINK":' || apex_json.stringify( ' ' ) ||-- apex_json.stringify( apex_mail.get_instance_url || apex_page.get_url( some_page_number )) ||
            '   ,"INVITEE":'             || apex_json.stringify( ' ' ) ||
            '   ,"EVENT_DATE":'          || apex_json.stringify( ' ' ) ||
            '   ,"ORGANIZER":'           || apex_json.stringify( ' ' ) ||
            '   ,"EVENT_LINK":'          || apex_json.stringify( ' ' ) ||
            '}' );
            
        apex_mail.push_queue;
        
        logger.log_info('Mail Sent with default password', l_scope, 'Mail Sent to = ' || p_user.username, l_params);

        logger.log('END', l_scope);
    EXCEPTION
      WHEN OTHERS THEN
      logger.log_error('Unhandled Exception', l_scope, dbms_utility.Format_error_backtrace(), l_params);
    END create_user;
    
END pkg_user_mgmt;    