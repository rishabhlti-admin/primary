CREATE TABLE int_candidate_document (
    int_candidate_document_id NUMBER
        GENERATED ALWAYS AS IDENTITY,
    document_name             VARCHAR2(40),
    candidate_id              NUMBER,
    document                  BLOB,
    last_updated              DATE,
    row_version               NUMBER NOT NULL,
    created                   DATE NOT NULL,
    created_by                VARCHAR2(50) NOT NULL,
    updated                   DATE NOT NULL,
    updated_by                VARCHAR2(50) NOT NULL,
    PRIMARY KEY ( int_candidate_document_id ),
    CONSTRAINT fk_document FOREIGN KEY ( candidate_id )
        REFERENCES int_candidate ( int_candidate_id )
            ON DELETE CASCADE
);