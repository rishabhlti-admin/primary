-- create tables
create table int_client_master (
    int_client_master_id           number generated by default on null as identity 
                                   constraint int_client_master_id_pk primary key,
    row_version                    integer not null,
    name                           varchar2(4000 char) not null,
    interview                      varchar2(4000 char),
    onsite_offsite                 varchar2(4000 char),
    created                        date not null,
    created_by                     varchar2(255 char) not null,
    updated                        date not null,
    updated_by                     varchar2(255 char) not null
)
;