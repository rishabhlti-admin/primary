CREATE TABLE int_candidate_experience (
    int_candidate_experience_id NUMBER
        GENERATED ALWAYS AS IDENTITY,
    candidate_id                NUMBER,
    experience                  NUMBER,
    company_name                VARCHAR2(100),
    start_date                  DATE,
    end_date                    DATE,
    row_version                 NUMBER NOT NULL,
    created                     DATE NOT NULL,
    created_by                  VARCHAR2(50) NOT NULL,
    updated                     DATE NOT NULL,
    updated_by                  VARCHAR2(50) NOT NULL,
    PRIMARY KEY ( int_candidate_experience_id ),
    CONSTRAINT fk_experience FOREIGN KEY ( candidate_id )
        REFERENCES int_candidate ( int_candidate_id )
            ON DELETE CASCADE
);